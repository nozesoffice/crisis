<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';

/*::: Dashboard Route :::*/
$route['dashboard'] = 'admin/dashboard';

/*::: Default Email template Route :::*/
$route['email_template'] = 'admin/email_template';

/*::: Edit Email template Route :::*/
$route['email_template/edit/(:any)/(:any)/(:any)/(:any)'] = 'admin/email_template/edit/$1/$2/$3/$4';

/*::: Email template Route :::*/
$route['email_template/index/(:any)/(:any)/(:any)'] = 'admin/email_template/index/$1/$2/$3';


/*::: Sorting Email template Route :::*/
$route['email_template/index/(:any)/(:any)'] = 'admin/email_template/index/$1/$2';

/*::: Restore Email template Route :::*/
$route['email_template/restore_template_value/(:any)/(:any)/(:any)/(:any)'] = 'admin/email_template/restore_template_value/$1/$2/$3/$4';

/*::: Preview Email template Route :::*/
$route['email_template/view_email_template/(:any)'] = 'admin/email_template/view_email_template/$1';

/*::: Send Test Email template Route :::*/
$route['email_template/send_test_email/(:any)'] = 'admin/email_template/send_test_email/$1';

/*::: Login Route :::*/
$route['login'] = 'admin/login';

/*Verify login details*/
$route['login/verify'] = 'admin/login/verify';

/*Logout*/
$route['logout'] = 'admin/login/logout';

/*Forgot Page*/
$route['forgot'] = 'admin/login/forgot_page';


/*Verify forgot details*/
$route['forgot/verify'] = 'admin/login/forgot_verify';

/*Reset Password Link*/
$route['reset_password_link/(:any)'] = 'admin/login/reset_password_link/$1';


/*Reset Password Page*/
$route['reset_password'] = 'admin/login/reset_page';


/*Update Password in Reset Password Page*/
$route['reset_password/update'] = 'admin/login/update_password';


/*Profile Page*/
$route['profile'] = 'admin/profile';

/*Update Profile Page*/
$route['profile/update'] = 'admin/profile/update';

/* Change Password */
$route['change_password'] = "admin/profile/change_password";


/*::: User Route :::*/
$route['users']      = 'admin/users';
$route['users/add']  = 'admin/users/add';
$route['users/edit/(:any)'] = 'admin/users/edit/$1';
$route['users/delete/(:any)'] = 'admin/users/delete/$1';
$route['users/lang'] = 'admin/users/lang';




/*::: User Roles :::*/
$route['roles']      = 'admin/roles';
$route['roles/add']  = 'admin/roles/add';
$route['roles/edit/(:any)'] = 'admin/roles/edit/$1';
$route['roles/delete/(:any)'] = 'admin/roles/delete/$1';
$route['roles/access/(:any)'] = 'admin/roles/access/$1';


/*::: CMS :::*/
$route['cms']               = 'admin/cms';
$route['cms/add']           = 'admin/cms/add';
$route['cms/edit/(:any)']   = 'admin/cms/edit/$1';
$route['cms/delete/(:any)'] = 'admin/cms/delete/$1';


$route['access_denied/(:any)'] = "Security/access_denied/$1";



$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['validation'] = 'Form';


