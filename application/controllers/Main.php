<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		Main
*	@Auther:		Rohit Singh
*	@Date:			25-10-2017
*	@Classname:		Main
*	@Description:	Main is default controller and send to dashboard .
*
*/

class Main extends CI_Controller 
{	
	/**
	* 
	* @function name : index()
	* @description   : It is default function of Main controller send to dashboard.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function index() 
	{
		redirect('dashboard');
	}

}
