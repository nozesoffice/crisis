<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		Security
*	@Auther:		Indrajit
*	@Date:			23-09-2017
*	@Classname:		Security
*	@Description:	Manage admin security functions.
*
*/

class Security extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->_init();
	}

	/**
	* 
	* @function name : _init()
	* @description   : initialize required resources in this view
	* @param   	 	 : void
	* @return        : void
	*
	*/
	private function _init() {
		// Set Template
		$this->output->set_template('admin_template');
		$admin_theme = $this->common->config('admin_theme').'/admin';
		$this->output->set_common_meta('Admin Dashboard','Codeigniter','This is admin dashboard page'); 
	}

	/**
	* 
	* @function name : index()
	* @description   : It is default function of Dashboard controller to load dashboard data.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function access_denied($Url='')
	{
		
		$admin_theme = $this->common->config('admin_theme');
		$content_page="themes/".$admin_theme."/access_denied";
		$this->data['url']=$this->functions->decode($Url);
		$this->load->view($content_page,$this->data);
		
	}
}
