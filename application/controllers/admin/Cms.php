<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		CMS
*	@Auther:		Rohit Singh
*	@Date:			12-02-2018
*	@Classname:		Cms
*	@Description:	Manage Roles
*
*/
class Cms extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('common','common');
		$this->load->model('admin/cms_model','cms');
		$this->lang->load('admin/cms_lang','english');
		$this->_init();
	}

	/**
	* 
	* @function name : _init()
	* @description   : initialize required resources in this view
	* @param   	 	 : void
	* @return        : void
	*
	*/
	private function _init() {
		// Set Template
		$this->output->set_template('admin_template');
		$admin_theme = $this->common->config('admin_theme').'/admin';
		$this->output->set_common_meta($this->lang->line('cms_title'),'Codeigniter','This is admin dashboard page'); 
	}

	/**
	* 
	* @function name : index()
	* @description   : It is default function of CMS controller to load CMS data.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function index() 
	{	
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login') ;
		}		

		$this->data['heading']      = $this->lang->line('cms_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class' => '',
		);
		$this->data['breadcrumb'][] = array(
			'text' => $this->lang->line('cms_heading'),
			'href' => base_url('cms'),
			'class' => 'active',
		);
		
		$this->data['cms_data'] = $this->db->get('static_page')->result_array();
		
		$admin_theme         = $this->common->config('admin_theme');        
		$content_page        = "themes/".$admin_theme."/admin/cms";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : add()
	* @description   : It is for adding new page in cms
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function add()
	{
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('seo_title', 'Seo title', 'required',array('required' => 'Enter Seo Title'));
        $this->form_validation->set_rules('title', 'Title', 'required',array('required' => 'Enter Title'));
        $this->form_validation->set_rules('description', 'Description', 'required',array('required' => 'Enter Description'));
       
	 	if ($this->form_validation->run() && $_SERVER['REQUEST_METHOD'] == "POST")
        {
    		
			$result = $this->cms->savecms();
			if($result)
			{
				$this->session->set_flashdata('success', 'Role has been added successfully.');
				redirect('cms');	
			}
			else
			{
				$this->session->set_flashdata('error', 'Role can`t add.');
				redirect('cms');	
			}    		     
    	}	

		$this->data['heading']      = $this->lang->line('cms_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => $this->lang->line('cms_heading'),
			'href' => base_url('cms'),
			'class'=>'',
		);

		$this->data['breadcrumb'][] = array(
			'text' => 'Add',
			'href' => '#',
			'class'=>'active',
		);

		$this->data['action'] = 'cms/add';

		$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/add_edit_cms";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : edit()
	* @description   : It is for edit role data
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function edit($id)
	{
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('seo_title', 'Seo title', 'required',array('required' => 'Enter Seo Title'));
        $this->form_validation->set_rules('title', 'Title', 'required',array('required' => 'Enter Title'));
        $this->form_validation->set_rules('description', 'Description', 'required',array('required' => 'Enter Description'));
       
	 	if ($this->form_validation->run() && $_SERVER['REQUEST_METHOD'] == "POST")
        {
			$result = $this->cms->updatecms($id);
			if($result)
			{
				$this->session->set_flashdata('success', 'CMS has been updated successfully.');
				redirect('cms');	
			}
			else
			{
				$this->session->set_flashdata('error', 'CMS can`t updated.');
				redirect('cms');	
			}    		     
    	}	

		$this->data['heading']      = $this->lang->line('cms_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => $this->lang->line('cms_heading'),
			'href' => base_url('cms'),
			'class'=>'',
		);

		$this->data['breadcrumb'][] = array(
			'text' => 'Edit',
			'href' => '#',
			'class'=>'active',
		);

		$this->data['action']    = 'cms/edit/'.$id;
		$this->data['edit_data'] = $this->cms->get_cms_data($id);

		$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/add_edit_cms";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : delete()
	* @description   : It is for delete cms page
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function delete($id)
	{
       	$result = $this->cms->delete($id);
        if($result)
        {
           $this->session->set_flashdata('success',"CMS Records Deleted successfully!");
        } else {        	
           $this->session->set_flashdata('error', "CMS Records can't deleted");
        }
       redirect('cms');
	}

	/**
	* 
	* @function name : action()
	* @description   : cms page active, deactive and delete functionality
	* @param   	 	 : void
	* @return        : void
	*
	*/
	function action()
	{
		if($this->input->post('action')=='action_active')
		{
			$this->cms->ChangeStatusSelected(1);
			$this->session->set_flashdata('success', 'Your selected CMS pages have been Published successfully.');
		}
		if($this->input->post('action')=='action_deactive')
		{
			$this->cms->ChangeStatusSelected(0);
			$this->session->set_flashdata('success', 'Your selected CMS pages have been Unpublished successfully.');
		}
		if($this->input->post('action')=='action_delete')
		{
			$this->cms->DeleteSelected();
			$this->session->set_flashdata('success', 'Your selected CMS pages have been Deleted successfully.');
		}
		redirect('cms');
	}			
}
