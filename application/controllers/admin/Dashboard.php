<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		Dashboard
*	@Auther:		Rohit Singh
*	@Date:			26-10-2017
*	@Classname:		Dashboard
*	@Description:	Manage admin dashboard .
*
*/

class Dashboard extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('common','common');
		$this->lang->load('admin/dashboard_lang','english');
		$this->_init();
	}

	/**
	* 
	* @function name : _init()
	* @description   : initialize required resources in this view
	* @param   	 	 : void
	* @return        : void
	*
	*/
	private function _init() {
		// Set Template
		$this->output->set_template('admin_template');
		$admin_theme = $this->common->config('admin_theme').'/admin';
		$this->output->set_common_meta($this->lang->line('dashboard_title'),'Codeigniter','This is admin dashboard page'); 
	}

	/**
	* 
	* @function name : index()
	* @description   : It is default function of Dashboard controller to load dashboard data.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function index() 
	{	

		if(!$this->session->userdata('logged_in'))
		{
			redirect('login') ;
		}
		$admin_theme = $this->common->config('admin_theme');        
		$content_page="themes/".$admin_theme."/admin/dashboard";

		$this->data['heading'] = $this->lang->line('dashboard_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i> Dashboard',
			'href' => base_url('dashboard'),
			'class' => 'active'
		);
		$this->load->view($content_page,$this->data);
	}
}
