<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		Login
*	@Auther:		Rohit Singh
*	@Date:			26-10-2017
*	@Classname:		Login
*	@Description:	Manage admin login .
*
*/
class Login extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('admin/login_model','login');
		$this->load->model('common','common');
		$this->lang->load('admin/auth_lang','english');
		$this->_init();
	}

	/**
	* 
	* @function name : _init()
	* @description   : initialize required resources in this view
	* @param   	 	 : void
	* @return        : void
	*
	*/
	private function _init() {
		// Set Template
		$this->output->set_template('admin/login/login_template');
		$admin_theme = $this->common->config('admin_theme').'/admin';
		$this->output->set_common_meta($this->lang->line('login_title'),'Codeigniter','This is admin login page');

		//Load Css and js
		$this->load->css(ADMIN_PATH.$admin_theme.'/bootstrap/css/bootstrap.min.css');		
		$this->load->css(ADMIN_PATH.$admin_theme.'/dist/css/AdminLTE.min.css');
		$this->load->css(ADMIN_PATH.$admin_theme.'/plugins/iCheck/square/blue.css');
	
		// Load JS
		$this->load->js(ADMIN_PATH.$admin_theme.'/plugins/jQuery/jquery-2.2.3.min.js');
		$this->load->js(ADMIN_PATH.$admin_theme.'/js/bootstrap.min.js');
		$this->load->js(ADMIN_PATH.$admin_theme.'/plugins/iCheck/icheck.min.js');		
	}

	/**
	* 
	* @function name : index()
	* @description   : It is default function of Login controller to check login data.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function index() 
	{
		if($this->session->userdata('logged_in'))
		{
			redirect('dashboard') ;
		}
		$admin_theme = $this->common->config('admin_theme');        
		$content_page="themes/".$admin_theme."/admin/login/login";

		$this->data['heading']              = $this->lang->line('login_heading');
		$this->data['email_placeholder']    = $this->lang->line('login_email_placeholder');
		$this->data['password_placeholder'] = $this->lang->line('login_password_placeholder');
		$this->data['forgot_text']          = $this->lang->line('login_forgot_text');
		$this->data['button_text']          = $this->lang->line('login_button_text');

		$this->load->view($content_page,$this->data);
	}


	/**
	* 
	* @function name : verify()
	* @description   : Verify function check inputted email and password for login.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function verify()
	{
		$email    = $this->input->post('email');
		$password = $this->input->post('password');

		$result   = $this->login->signin($email);

		if($result->num_rows() > 0)
		{
			$data   = $result->row_array();

			if (!password_verify($password, $data['password'])) 
			{
				$this->session->set_flashdata('error',$this->lang->line('login_invalid_msg'));
				redirect('login');	    		
			}			
			if($data['active'] == 0)
			{
				$this->session->set_flashdata('error',$this->lang->line('account_deactivated_msg'));
			}
			else
			{
				$session_data = array(
					'uid'         => $data['id'],
					'email'       => $data['email'],
					'role_id'     => $data['tbl_roles_role_id'],
					'lang'     => $data['selected_lang'],
					'logged_in'   => TRUE,
				);
				$this->session->set_userdata($session_data) ;
				$this->rbac->SetAccessInSession();
				redirect('dashboard');
			}
		}
		else
		{
			$this->session->set_flashdata('error',$this->lang->line('login_invalid_msg'));
		}
		redirect('login');
	}

	/**
	* 
	* @function name : forgot_page()
	* @description   : forgot_page function load forgot page.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function forgot_page() 
	{
		$admin_theme = $this->common->config('admin_theme');        
		$content_page="themes/".$admin_theme."/admin/forgot";
		$this->output->set_common_meta($this->lang->line('forgot_title'),'Codeigniter','This is admin forgot page');

		$this->data['heading']             = $this->lang->line('forgot_heading');
		$this->data['email_placeholder']   = $this->lang->line('login_email_placeholder');
		$this->data['login_text']          = $this->lang->line('login_text');
		$this->data['reset_button_text']   = $this->lang->line('reset_button_text');

		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : forgot_verify()
	* @description   : Verify function check inputted email and send link.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function forgot_verify()
	{
		$email    = $this->input->post('email');
		$result   = $this->login->check_email($email);
		$userdata = $result->row_array();
		$username = $userdata['first_name']." ".$userdata['last_name'];

		if(empty($email))
		{
			$this->session->set_flashdata('error',$this->lang->line('email_required_msg'));
		}
		else if($result->num_rows() > 0)
		{
			$reset_link  =  md5($email.time());
			$set_forgot  =  $this->login->update_forgot_password_field($email,$reset_link) ;
			$mail_result =  $this->send_reset_link($username,$email,$reset_link) ;

			if($mail_result)
			{
				$this->session->set_flashdata('success',$this->lang->line('email_sent_msg'));
			}
			else
			{
				$this->session->set_flashdata('error',$this->lang->line('email_failed_msg'));
			}
		}
		else
		{
			$this->session->set_flashdata('error',$this->lang->line('email_not_found_msg'));
		}
		redirect('forgot');
	}

	/**
	* 
	* @function name : send_reset_link()
	* @description   : send_reset_link function send link to email.
	* @param   	 	 : email, reset_link
	* @return        : void
	*
	*/
	function send_reset_link($username,$email,$reset_link)
    {
        $template_code = 'forgot_password';
        $TPL           = $this->login->getEmailTemplateByCode($template_code);
        $Template      = $this->mailer->Tpl_Email($TPL['template_code'],$email);
        $Recipient     = $email;
        $Filter        = array(
	                        '{{NAME}}'       => $username,
	                        '{{RESET_LINK}}' => base_url('reset_password_link/'.$reset_link),
	                    	);
        $sent_result   = $this->mailer->Send_Singal_Html_Email($Recipient,$Template,$Filter);

        if($sent_result)
        {
           return true ;
        }
        else
        {
            return false ;
        }
    }

	/**
	* 
	* @function name : reset_password_link()
	* @description   : check link come from email and send to reset page
	* @param   	 	 : link
	* @return        : void
	*
	*/
	public function reset_password_link($link)
	{
		if(!empty($link))
		{
			$result = $this->login->check_reset_link($link);
			$email  = $result['email'] ;

			if(!empty($result))
			{	
				$this->session->set_userdata('reset_email_id',$email);
				$this->reset_page();
			}
			else
			{
				$this->session->set_flashdata('error',$this->lang->line('reset_link_expired_msg'));
				redirect('login');
			}
		}
	}

	/**
	* 
	* @function name : reset_page()
	* @description   : reset_page function load Reset Password page.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function reset_page() 
	{
		if(!$this->session->userdata('reset_email_id'))
		{
			redirect('login');
		}

		$admin_theme = $this->common->config('admin_theme');        
		$content_page="themes/".$admin_theme."/admin/reset_password";
		$this->output->set_common_meta($this->lang->line('reset_password_title'),'Codeigniter','This is admin login page');
		$this->data['heading']                         = $this->lang->line('reset_password_heading');
		$this->data['new_password_placeholder']        = $this->lang->line('new_password_placeholder');
		$this->data['confirm_new_password_placeholder']= $this->lang->line('confirm_new_password_placeholder');
		$this->data['login_text']                      = $this->lang->line('login_text');
		$this->data['update_password_button_text']     = $this->lang->line('update_password_button_text');
		$this->load->view($content_page,$this->data);
	}

	
	/**
	* 
	* @function name : update_password()
	* @description   : Update password with new password.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function update_password() 
	{
		$new_password         = $this->input->post('new_password');
		$confirm_new_password = $this->input->post('cnew_password');

		if(empty($new_password) || empty($confirm_new_password))
		{
			$this->session->set_flashdata('error',$this->lang->line('reset_password_required_msg'));
		}
		else
		{
			if($confirm_new_password != $new_password)
			{
			$this->session->set_flashdata('error',$this->lang->line('reset_password_not_match_msg'));
			}
			else
			{
				$hash_password  = password_hash($new_password, PASSWORD_DEFAULT);
				$reset_email_id = $this->session->userdata('reset_email_id') ;
				$result         = $this->login->update_new_password($hash_password,$reset_email_id);
				if($result)
				{
					$this->login->update_forgot_password_field($reset_email_id,$reset_link='',$password_updated = true);
					$this->session->unset_userdata('reset_email_id');
					$this->session->set_flashdata('success',$this->lang->line('password_updated_msg'));
					redirect('login') ;
				}
				else
				{
					$this->session->set_flashdata('error',$this->lang->line('password_not_updated_msg'));
				}				
			}
		}
		redirect('reset_password') ;
	}

	/**
	* 
	* @function name : logout()
	* @description   : logout function logout user from site.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

}
