<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		Profile
*	@Auther:		Rohit Singh
*	@Date:			26-10-2017
*	@Classname:		Profile
*	@Description:	Manage admin profile data.
*
*/
class Profile extends CI_Controller 
{
	private $data=array();
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('admin/profile_model','profile');
		$this->lang->load('admin/changepass_lang','english');
		$this->load->model('common','common');
		$this->_init();
	}

	/**
	* 
	* @function name : _init()
	* @description   : initialize required resources in this view
	* @param   	 	 : void
	* @return        : void
	*
	*/
	private function _init() {
		// Set Template
		$this->output->set_template('admin_template');
		$admin_theme = $this->common->config('admin_theme').'/admin';
		$this->output->set_common_meta('Admin Profile','Codeigniter','This is admin profile page'); 
	}

	/**
	* 
	* @function name : index()
	* @description   : It is default function of Profile controller to load profile data.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function index() 
	{	
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login') ;
		}	
		
		$this->data['user_details'] = $this->common->get_userdata($this->session->userdata('email'));
		$admin_theme                = $this->common->config('admin_theme');				
		$content_page               = "themes/".$admin_theme."/admin/profile";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : update()
	* @description   : Update user profile data with password and images.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function update()
	{
		$this->form_validation->set_rules('user_name','Username','required|trim|xss_clean|callback_check_username');
		$this->form_validation->set_rules('first_name','First Name','required|trim|xss_clean');
		$this->form_validation->set_rules('last_name','Last Name','required|trim|xss_clean');
		$this->form_validation->set_rules('contact','Contact','required|trim|xss_clean|callback_check_phone');

		if ($_SERVER['REQUEST_METHOD'] == 'POST'){			

			if($this->form_validation->run())
			{
				$email  = $this->session->userdata('email');
				if($_FILES['userfile']['name'])
				{
					$profile_img = $this->do_upload() ;
				}
				else
				{
					$profile_img = '';
				}
				$result = $this->profile->update_profile($email,$profile_img);
				if($result)
				{
					$this->session->set_flashdata('success','Profile updated successfully.');
				}
				else
				{
					$this->session->set_flashdata('error','Profile can`t updated.');
				}				
			}
			else
			{
				$this->session->set_flashdata('error',validation_errors());
			}			
		}
		redirect('profile');
	}

	/**
	* 
	* @function name : do_upload()
	* @description   : private function for uploading image.
	* @param   	 	 : void
	* @return        : void
	*
	*/

	private function do_upload()
    {
    	if(!is_dir('uploads/profile'))
        {
            mkdir('./uploads/profile', 0777, true);
        }

        $config['upload_path']          = './uploads/profile/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 1000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        $config['overwrite']            = TRUE;
        $config['file_name']            = 'profile_'.$this->session->userdata('uid').'.png';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('userfile'))
        {
            $this->session->set_flashdata('error',$this->upload->display_errors());
            redirect('profile') ;
        }
        else
        {
            return $this->upload->data('file_name');
        }
    }


	/**
	* 
	* @function name : change_password()
	* @description   : Change account password
	* @param   	 	 : void
	* @return        : void
	*
	*/
    public function change_password()
	{
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login') ;
		}

		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class' => ''
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Change password',
			'href' => '#',
			'class' => 'active'
		);	
		
		if($this->input->post('submit'))
		{		
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			$this->form_validation->set_rules('old_password', 'old password', 'required|min_length[8]');
			$this->form_validation->set_rules('new_password', 'new password', 'required|min_length[8]');
			$this->form_validation->set_rules('confirm_new_password', 'confirm password', 'required|matches[new_password]');
			
			if($this->form_validation->run())
			{	
				$result=$this->profile->Change_Password();
				if($result)
				{
					$this->session->set_flashdata('success', 'Your new password set successfully.');
				}
				else
				{
					$this->session->set_flashdata('error', 'Please enter valid old password');
				}
				redirect('change_password');	
			}
		}
		
		$this->output->set_common_meta($this->lang->line('changepass_title'),'Codeigniter','This is Change Password page'); 
		$this->data['title']      = $this->lang->line('changepass_title');
    	$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/change_password";		
		$this->load->view($content_page, $this->data);
	}	

	/**
	* 
	* @function name : check_username()
	* @description   : Check for unique username
	* @param   	 	 : void
	* @return        : void
	*
	*/
	function check_username() 
	{  
		$user_id = $this->input->post('hidden_user_id');
		$username = $this->input->post('user_name');

	    $result = $this->profile->check_unique_user_username($user_id, $username);
	    if($result == 0)
	    { $response = true; }
	    else 
	    {
	        $this->form_validation->set_message('check_username', 'Entered Username is already Exist');
	        $response = false;
	    }
	    return $response;
	}

	/**
	* 
	* @function name : check_phone()
	* @description   : Check for unique phone
	* @param   	 	 : void
	* @return        : void
	*
	*/
	function check_phone() 
	{  
		$user_id = $this->input->post('hidden_user_id');
		$phone = $this->input->post('contact');

	    $result = $this->profile->check_unique_user_phone($user_id, $phone);
	    if($result == 0)
	    { $response = true; }
	    else 
	    {
	        $this->form_validation->set_message('check_phone', 'Entered Phone is already Exist');
	        $response = false;
	    }
	    return $response;
	}
}
