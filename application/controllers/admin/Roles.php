<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		Roles
*	@Auther:		Rohit Singh
*	@Date:			12-02-2018
*	@Classname:		Roles
*	@Description:	Manage Roles
*
*/

class Roles extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('common','common');
		$this->load->model('admin/role_model','role');
		$this->lang->load('admin/role_lang','english');
		$this->_init();
	}

	/**
	* 
	* @function name : _init()
	* @description   : initialize required resources in this view
	* @param   	 	 : void
	* @return        : void
	*
	*/
	private function _init() {
		// Set Template
		$this->output->set_template('admin_template');
		$admin_theme = $this->common->config('admin_theme').'/admin';
		$this->output->set_common_meta($this->lang->line('role_title'),'Codeigniter','This is admin dashboard page'); 
	}

	/**
	* 
	* @function name : index()
	* @description   : It is default function of Dashboard controller to load dashboard data.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function index() 
	{	
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login') ;
		}		

		$this->data['heading']      = $this->lang->line('role_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class' => '',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Roles',
			'href' => base_url('roles'),
			'class' => 'active',
		);
		
		$this->data['roles'] = $this->db->where('role_id != 1')->get('tbl_roles')->result_array();
		
		$admin_theme         = $this->common->config('admin_theme');        
		$content_page        = "themes/".$admin_theme."/admin/roles";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : add()
	* @description   : It is for adding new role
	* @param   	 	 : void
	* @return        : void
	*
	*/

	public function add()
	{
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('role_name', 'Role name', 'required',array('required' => 'Enter Role name'));

        $this->form_validation->set_rules('role_status', 'Role Status', 'required',array('required' => 'Enter Select Status'));

        
	 	if ($this->form_validation->run() && $_SERVER['REQUEST_METHOD'] == "POST")
        {
    		
			$result = $this->role->saverole();
			if($result)
			{
				$this->session->set_flashdata('success', 'Role has been added successfully.');
				redirect('admin/roles');	
			}
			else
			{
				$this->session->set_flashdata('error', 'Role can`t add.');
				redirect('admin/roles');	
			}
    		    		     
    	}	

		$this->data['heading']      = $this->lang->line('role_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Roles',
			'href' => base_url('roles'),
			'class'=>'',
		);

		$this->data['breadcrumb'][] = array(
			'text' => 'Add',
			'href' => '#',
			'class'=>'active',
		);

		$this->data['action'] = 'roles/add';

		$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/add_edit_roles";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : edit()
	* @description   : It is for edit role data
	* @param   	 	 : void
	* @return        : void
	*
	*/

	public function edit($role_id)
	{
		if($role_id == 1)
		{
			$this->session->set_flashdata('error', 'You can`t update this role.');
			redirect('admin/roles');
		}
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('role_name', 'Role name', 'required',array('required' => 'Enter Role name'));

        $this->form_validation->set_rules('role_status', 'Role Status', 'required',array('required' => 'Enter Select Status'));

        
	 	if ($this->form_validation->run() && $_SERVER['REQUEST_METHOD'] == "POST")
        {
    		
			$result = $this->role->updaterole();

			if($result)
			{
				$this->session->set_flashdata('success', 'Role has been updated successfully.');
				redirect('admin/roles');	
			}
			else
			{
				$this->session->set_flashdata('error', 'Role can`t update.');
				redirect('admin/roles');	
			}
    		    		     
    	}

		$this->data['heading']      = $this->lang->line('role_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Roles',
			'href' => base_url('roles'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Edit',
			'href' => '#',
			'class'=>'active',
		);

		$this->data['action']    = 'roles/edit/'.$role_id;
		$this->data['edit_data'] = $this->role->get_role_data($role_id);
		
		$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/add_edit_roles";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : delete()
	* @description   : It is for delete  role data
	* @param   	 	 : role id
	* @return        : void
	*
	*/	

    public function delete($role_id)
    {
    	if($role_id == 1)
		{
			$this->session->set_flashdata('error', 'You can`t delete this role.');
			redirect('admin/roles');
		}

       	$result = $this->role->delete($role_id);
        
        if($result)
        {
           $this->session->set_flashdata('success',"Role Records Deleted successfully!");
        } else {        	
           $this->session->set_flashdata('error', "Role Records can't deleted");
        }
       redirect('roles');
    }

    /**
	* 
	* @function name : access()
	* @description   : It is for assigning rights to role
	* @param   	 	 : role id
	* @return        : void
	*
	*/	

    public function access($role_id)
    {
    	$get_role_data = $this->role->get_role_data($role_id);
    	if(empty($get_role_data))
    	{
    		$this->session->set_flashdata('error','Sorry ! Role Not found.');
    		redirect('roles');

    	}
    	if($this->input->post('submit'))
		{ 
			$res = $this->role->SetAccess();
			if($res)
			{
				$this->session->set_flashdata('success', 'Role Access Updated Successfully');	
			}
			else
			{
				$this->session->set_flashdata('error','Role Access Can`t Updated');
			}
			redirect('roles');
		}


       	$this->data['heading']      = $this->lang->line('role_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Roles',
			'href' => base_url('roles'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Access',
			'href' => '#',
			'class'=>'active',
		);

		$this->data['action']         = 'roles/access/'.$role_id;
		$this->data['role_data']      = $get_role_data;
		$this->data['system_modules'] = $this->role->get_system_modules();
        $this->data['group_access']   = $this->role->GetAccess($role_id);
		
		$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/system_modules";
		$this->load->view($content_page,$this->data);
    }

    function action()
	{
		if($this->input->post('action')=='action_active')
		{
			$this->role->ChangeStatusSelected(1);
			$this->session->set_flashdata('success', 'Your selected User roles have been Active successfully.');
		}
		if($this->input->post('action')=='action_deactive')
		{
			$this->role->ChangeStatusSelected(0);
			$this->session->set_flashdata('success', 'Your selected User roles have been Deactive successfully.');
		}
		if($this->input->post('action')=='action_delete')
		{
			$this->role->DeleteSelected();
			$this->session->set_flashdata('success', 'Your selected User roles have been Deleted successfully.');
		}
		redirect('roles');
	}			


}
