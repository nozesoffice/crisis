<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		User
*	@Auther:		Rohit Singh
*	@Date:			10-02-2018
*	@Classname:		User
*	@Description:	Manage Users
*
*/

class Users extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->rbac->CheckAuthentication();

		$this->load->model('common','common');
		$this->load->model('admin/user_model','user');
		$this->lang->load('admin/user_lang',$this->session->userdata('lang'));
		$this->_init();
	}

	/**
	* 
	* @function name : _init()
	* @description   : initialize required resources in this view
	* @param   	 	 : void
	* @return        : void
	*
	*/
	private function _init() {
		// Set Template
		$this->output->set_template('admin_template');
		$admin_theme = $this->common->config('admin_theme').'/admin';
		$this->output->set_common_meta($this->lang->line('user_title'),'Codeigniter','This is admin dashboard page'); 
	}

	/**
	* 
	* @function name : index()
	* @description   : It is default function of Dashboard controller to load dashboard data.
	* @param   	 	 : void
	* @return        : void
	*
	*/
	public function index() 
	{	
		$this->rbac->CheckModuleAccess();
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login') ;
		}

		

		$this->data['heading']      = $this->lang->line('user_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class' => ''
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Users',
			'href' => '#',
			'class' => 'active'
		);

		$this->data['users'] = $this->user->all_users();

		$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/users";		
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : add()
	* @description   : It is for adding new user
	* @param   	 	 : void
	* @return        : void
	*
	*/

	public function add()
	{
		$this->rbac->CheckOperationAccess();

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('first_name', 'First name', 'required',array('required' => 'Enter first name'));

        $this->form_validation->set_rules('last_name', 'Last name', 'required',array('required' => 'Enter last name'));

        $this->form_validation->set_rules('user_role', 'Role', 'required',array('required' => 'Please select any one user role'));

   	 	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|min_length[4]|is_unique[tbl_users.email]',
        array('required' => 'Enter Email ID','valid_email' => 'Enter Valid Email Id'));        
	 	$this->form_validation->set_message('is_unique', 'The Email is already exists.');

	 	$this->form_validation->set_rules('user_name', 'Username', 'required|is_unique[tbl_users.username]',array('required' => 'Enter Username','is_unique'=>'Username already taken.'));

	 	$this->form_validation->set_rules('phone', 'Contact', 'required|numeric|is_unique[tbl_users.phone]',array('required' => 'Enter Contact Number','is_unique'=>'Contact Number already exists.','numeric'=>'Only digits are allowed.'));

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]',array('required' => 'Enter Password','min_length' => 'Password should be atleast 8 characters.'));

	 	$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]',array('required' => 'Enter Confirm password'));
	 	$this->form_validation->set_message('matches','Confirm Password does not match Password');
	 	
	 	if ($this->form_validation->run() && $_SERVER['REQUEST_METHOD'] == "POST")
        {
    		
			$result = $this->user->saveuser();
			if($result)
			{
				$this->session->set_flashdata('success', 'User has been added successfully.');
				redirect('admin/users');	
			}
			else
			{
				$this->session->set_flashdata('error', 'User can`t add.');
				redirect('admin/users');	
			}
    		    		     
    	}		

		$this->data['heading']      = $this->lang->line('user_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Users',
			'href' => base_url('users'),
			'class'=>'',
		);

		$this->data['breadcrumb'][] = array(
			'text' => 'Add',
			'href' => '#',
			'class'=>'active',
		);

		$this->data['action'] = 'users/add';
		$this->data['users']  = $this->user->all_users();
		$this->data['roles']  = $this->db->where('role_id != 1')->get('tbl_roles')->result_array();

		$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/add_edit_user";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : edit()
	* @description   : It is for edit  user data
	* @param   	 	 : void
	* @return        : void
	*
	*/

	public function edit($user_id)
	{
		$this->rbac->CheckOperationAccess();
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('first_name', 'First name', 'required',array('required' => 'Enter first name'));

        $this->form_validation->set_rules('last_name', 'Last name', 'required',array('required' => 'Enter last name'));

        $this->form_validation->set_rules('user_role', 'Role', 'required',array('required' => 'Please select any one user role'));

   	 	$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email|valid_email|min_length[4]',
        array('required' => 'Enter Email ID','valid_email' => 'Enter Valid Email Id'));        
	 	

	 	$this->form_validation->set_rules('user_name', 'Username', 'required|callback_check_username',array('required' => 'Enter Username'));

	 	$this->form_validation->set_rules('phone', 'Contact', 'required|numeric|callback_check_phone',array('required' => 'Enter Contact Number','numeric'=>'Only digits are allowed.'));

        $this->form_validation->set_rules('password', 'Password', 'min_length[8]',array('min_length' => 'Password should be atleast 8 characters.'));

	 	$this->form_validation->set_rules('cpassword', 'Confirm Password', 'matches[password]');
	 	$this->form_validation->set_message('matches','Confirm Password does not match Password');
	 	
	 	if ($this->form_validation->run() && $_SERVER['REQUEST_METHOD'] == "POST")
        {    		
			$result = $this->user->updateuser();

			if($result)
			{
				$this->session->set_flashdata('success', 'User has been updated successfully.');
				redirect('admin/users');	
			}
			else
			{
				$this->session->set_flashdata('error', 'User can`t be updated.');
				redirect('admin/users');	
			}    		     		     
    	}

		$this->data['heading']      = $this->lang->line('user_heading');
		$this->data['breadcrumb'][] = array(
			'text' => '<i class="fa fa-dashboard"></i>Dashboard',
			'href' => base_url('dashboard'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Users',
			'href' => base_url('users'),
			'class'=>'',
		);
		$this->data['breadcrumb'][] = array(
			'text' => 'Edit',
			'href' => '#',
			'class'=>'active',
		);

		$this->data['action']    = 'users/edit/'.$user_id;
		$this->data['edit_data'] = $this->user->get_user_data($user_id);
		$this->data['users']     = $this->user->all_users();
		$this->data['roles']     = $this->db->where('role_id != 1')->get('tbl_roles')->result_array();

		$admin_theme  = $this->common->config('admin_theme');        
		$content_page ="themes/".$admin_theme."/admin/add_edit_user";
		$this->load->view($content_page,$this->data);
	}

	/**
	* 
	* @function name : custom_check()
	* @description   : It is for checking existing field value from database
	* @param   	 	 : void
	* @return        : void
	*
	*/

	function check_username() 
	{  
		$user_id = $this->input->post('hidden_user_id');
		$username = $this->input->post('user_name');

	    $result = $this->user->check_unique_user_username($user_id, $username);
	    if($result == 0)
	    { $response = true; }
	    else 
	    {
	        $this->form_validation->set_message('check_username', 'Entered Username is already Exist');
	        $response = false;
	    }
	    return $response;
	}

	function check_email() 
	{  
		$user_id = $this->input->post('hidden_user_id');
		$email = $this->input->post('email');

	    $result = $this->user->check_unique_user_email($user_id, $email);
	    if($result == 0)
	    { $response = true; }
	    else 
	    {
	        $this->form_validation->set_message('check_email', 'Entered Email is already Exist');
	        $response = false;
	    }
	    return $response;
	}

	function check_phone() 
	{  
		$user_id = $this->input->post('hidden_user_id');
		$phone = $this->input->post('phone');

	    $result = $this->user->check_unique_user_phone($user_id, $phone);
	    if($result == 0)
	    { $response = true; }
	    else 
	    {
	        $this->form_validation->set_message('check_phone', 'Entered Phone is already Exist');
	        $response = false;
	    }
	    return $response;
	}

	/**
	* 
	* @function name : delete()
	* @description   : It is for delete  user data
	* @param   	 	 : $user_id
	* @return        : void
	*
	*/	

    public function delete($user_id)
    {
    	$this->rbac->CheckOperationAccess();
        if (!empty($user_id)) {
            $result = $this->user->delete($user_id);
        }
        if($result)
        {
           $this->session->set_flashdata('success',"User Records Deleted successfully!");
        } else {        	
           $this->session->set_flashdata('error', "User Records can't deleted");
        }
       redirect('users');
    }

    /**
	* 
	* @function name : action()
	* @description   : User active, deactive and delete functionality
	* @param   	 	 : void
	* @return        : void
	*
	*/
	function action()
	{
		if($this->input->post('action')=='action_active')
		{
			$this->user->ChangeStatusSelected(1);
			$this->session->set_flashdata('success', 'Your selected Users have been Active successfully.');
		}
		if($this->input->post('action')=='action_deactive')
		{
			$this->user->ChangeStatusSelected(0);
			$this->session->set_flashdata('success', 'Your selected Users have been Deactive successfully.');
		}
		if($this->input->post('action')=='action_delete')
		{
			$this->user->DeleteSelected();
			$this->session->set_flashdata('success', 'Your selected Users have been Deleted successfully.');
		}
		redirect('users');
	}	


	public function lang()		
	{
		$user_id=$this->session->userdata('uid');
		$data = array(
			'selected_lang'        => $this->input->post('key')
		);
		$result = $this->db->where('id',$user_id)->update('tbl_users',$data);
		$session_data = array(
					'lang'     => $data['selected_lang'],
				);
		$this->session->set_userdata($session_data);
		echo json_encode($result);
		
	}

}
