<?php
require(APPPATH.'libraries/REST_Controller.php');

// use namespace
//use Restserver\Libraries\REST_Controller;
 
class Security extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Security_model', 'securitymodel');
        $this->load->model('api/Users_model', 'users');
    }
    

    /**
    * 
    * @function name : registration_post()
    * @description   : New user registration
    * @access        : public
    * @Method        : post
    * @url           : http://localhost/ci-rbac-demo/api/users/registration
    * @return        : response
    *
    */
    function registration_post()
    {
        $json =array();
        $validation = array(
                               array(
                                'field' => 'username',
                                'label' => 'Username', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|callback_check_exits_username|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!','check_exits_username'=>'Username is already exists')
                                    ),
                              array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|callback_check_exits_email|xss_clean', 
                                'errors' => array('required' => '%s is required!','check_exits_email'=>'%s is already exists')
                                    ),
                              array(
                                'field' => 'password',
                                'label' => 'Password', 
                                'rules' => 'trim|required|min_length[6]', 
                                'errors' => array('required' => '%s is required!')
                                    ),
                              array(
                                'field' => 'cpassword',
                                'label' => 'Confirm password', 
                                'rules' => 'trim|required|min_length[6]|matches[password]', 
                                'errors' => array('required' => '%s is required!','matches' => 'Password and confirm password are not match')
                                    ),
                                array(
                                    'field' => 'userfile',
                                    'label' => 'Image',
                                    'rules' => 'callback_file_check'
                                    ),
                            );
                    
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();
            $error_list = implode(" | ", $errors);
            log_message('error', "Check for this errors :- ".$error_list);

            $json['response'] = false;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Please check the form carefully for errors!";
        }
        else
        {
            log_message('error', 'Validation true');
            $json['response'] = true;
            if(isset($_FILES['userfile']['name']))
            {
                $profile_img = $this->do_upload();
            }
            else
            {
                $profile_img = '';
            }

            $email       = $this->post('email');
            $password    = $this->post('password');
            $username    = $this->post('username');

            $data = array(
                'email'         => $email,
                'password'      => $password,
                'role_id'       => 2,
                'username'      => $username,
                'userfile'     => $profile_img,
                );
            log_message('error', 'Make array for post variables');
            $res = $this->securitymodel->Register($data);
            log_message('error', 'Insert data in database via load modal');
            if($res)
            {
                $user_info = $this->users->getUserById($res);
                log_message('error', 'get user data for sent it in response');
                if(!empty($user_info['userimage']))
                {
                    $path = FCPATH."uploads/profile/".$user_info['userimage'];
                    if(file_exists($path) && !empty($user_info['userimage']))
                    {
                        $user_info['userimage'] = base_url('uploads/profile/'.$user_info['userimage']);
                    }
                    else
                    {
                        $user_info['userimage'] = base_url('assets/themes/default/images/default_image/male.png');
                    }
                    log_message('error', 'Make Full path for image');

                }

                $json['success'] ="Register successfully done";     
                $json['data'] = $user_info;
            }
            else
            {
                log_message('error', 'registration error');
                $json['response'] = false;
                $json['error'] ='Something went wrong!';
            }
        }
        $this->response($json,200);
    }

    public function file_check($str)
    {
        log_message('error', 'In a file check function');
        if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['name']!="")
        {
            log_message('error', 'set $_FILES variables');
            $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
            $mime = get_mime_by_extension($_FILES['userfile']['name']);
            if(in_array($mime, $allowed_mime_type_arr))
            {
                log_message('error', 'check mime for file');
                if($_FILES['userfile']['size'] > 2097152)
                {
                    $this->form_validation->set_message('file_check', 'Please select file with maximum size of 2Mb.');
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                log_message('error', 'Image file only');
                $this->form_validation->set_message('file_check', 'Please select only image file.');
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    /**
    * 
    * @function name : do_upload()
    * @description   : private function for uploading image.
    * @param         : void
    * @return        : void
    *
    */

    private function do_upload()
    {
        log_message('error', 'In a do_upload function');
        if(!is_dir('uploads/profile'))
        {
            log_message('error', 'dir created');
            mkdir('./uploads/profile', 0777, true);
        }
        $config['upload_path']          = './uploads/profile/';
        $config['allowed_types']        = '*';
        $config['max_size']             = 2048;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $config['overwrite']            = FALSE;
        $config['file_name']            = 'profile'.date("YmdHi").'.png';
        log_message('error', 'Manage all the config for image');
        $this->load->library('upload', $config);
        log_message('error', 'Load upload librarie');
        $this->upload->initialize($config);
        log_message('error', 'initialize config');
        if ( ! $this->upload->do_upload('userfile'))
        {
            log_message('error', 'File upload error');
            $file_size = $this->upload->data('file_size');
            if($file_size > 242424)
            {
                $error = "You cannot upload file more than 2MB size";
            }
            else
            {
                $error = $this->upload->display_errors();
            }
            log_message('error', "Check for following errors :- ". $error);       
        }
        else
        {
            log_message('error', 'File upload successfully');
            return $this->upload->data('file_name');
        }
    }

    /**
    * 
    * @function name : loginUser_post
    * @description   : login users
    * @access        : public
    * @HTTP Method   : post
    * @return        : json
    * @url           : base_url().api/users/loginUser_post
    */
 
    function loginUser_post()
    {
        $json =array();
        $validation = array(
                            array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|xss_clean', 
                                'errors' => array('required' => '%s is required!','check_exits_email'=>'%s is already exists')
                                ),
                            array(
                                'field' => 'password',
                                'label' => 'Password', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => '%s is required!')
                                ),

                            );
                    
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();
            $error_list = implode(" | ", $errors);
            log_message('error', "Check for this errors :- ".$error_list);

            $json['response'] = false;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
        }
        else
        {
            log_message('error', 'Validation true');
            $json['response'] = true;
            
            $email       = $this->post('email');
            $password    = $this->post('password');

            $data = array(
                'email'      => $email,
                'password'   => $password,
                );

            $result = $this->securitymodel->Login($data);
    
            if($result)
            {
                log_message('error', 'user find');
                if (!password_verify($password, $result['password'])) 
                {
                    log_message('error', 'invalid password');
                    $json['response'] = false;
                    $json['error'] ='Invalid email/password!';          
                }           
                else if($result['active'] == 0)
                {
                    log_message('error', 'Your account is not actived yet');
                    $json['response'] = false;
                    $json['error'] ='Your account is not actived yet';
                }
                else
                {
                    if(!empty($result['userimage'])){
                        log_message('error', 'if file is not empty');
                        $path = FCPATH."uploads/profile/".$result['userimage'];
                        if(file_exists($path) && !empty($result['userimage']))
                        {
                            log_message('error', 'upload file');
                            $result['userimage'] = base_url('uploads/profile/'.$result['userimage']);
                        }
                        else
                        {
                            log_message('error', 'set default image');
                            $result['userimage'] = base_url('assets/themes/default/images/default_image/male.png');
                        }

                    }
    
                    $json['error'] ="Success: you have been successfully logged in!";
                    $json['data'] = $result;
                }   
            }
            else
            {
                log_message('error', 'invalid email or password');
                $json['response'] = false;
                $json['error'] ='Invalid email/password!';
            }

        }
        $this->response($json,200);
    } 
    /**
    * 
    * @function name : check_exits_username
    * @description   : Check exiting email in customer data
    * @param1        : string
    * @return        : void
    *
    */
    function check_exits_username($str)
    {
            log_message('error', 'in check_exits_username function');
            $this->db->from('tbl_users');
            $this->db->where('username',$this->post('username'));
            if($this->post('id') !=="")
            {
              log_message('error', 'getting data from id');
              $this->db->where('id !=',(int)$this->post('id'));  
            }
            $query=$this->db->get();
            $row = $query->num_rows();
            if($row > 0)
            {
                log_message('error', 'check for existing username return false');
                return false;
            }
            else
            {
                log_message('error', 'check for existing username return true');
                return true;
            }   
    }
    
    /**
    * 
    * @function name : check_exits_email
    * @description   : Check exiting email in customer data
    * @param1        : string
    * @return        : void
    *
    */
    function check_exits_email($str)
    {
            log_message('error', 'in check_exits_email function');
            $this->db->from('tbl_users');
            $this->db->where('email',$this->post('email'));
            if($this->post('id') !=="")
            {
              $this->db->where('id !=',(int)$this->post('id'));  
            }
            $query=$this->db->get();
            $row = $query->num_rows();
            if($row > 0)
            {
                return false;
            }
            else
            {
                return true;
            }   
    }

    /**
    * 
    * @function name : forgotPassword_post
    * @description   : forgot password request by user
    * @access        : public
    * @HTTP Method   : post
    * @return        : json
    * @url           : base_url().api/security/forgotPassword_post
    */
    function forgotPassword_post()
    {
        log_message('error', 'in forgotPassword function');
        $json =array();
        $validation = array(
                            array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|xss_clean', 
                                'errors' => array('required' => '%s is required!')
                            ),
                        );
                    
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();
            $error_list = implode(" | ", $errors);
            log_message('error', "Check for this errors :- ".$error_list);

            $json['response'] = false;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
        }
        else
        { 
            log_message('error', 'validation true');
            $email       = $this->post('email');
            $data = array(
                'email'      => $email,
                );

            $result = $this->securitymodel->forgotPassword($data);
            if($result)
            {   log_message('error', 'getting result');
                $session_data = array(
                    'reset_email_id' => $email,
                );
                $this->session->set_userdata($session_data);
                log_message('error', 'An email with a confirmation link has been sent your Register email address');
                $json['response'] = true;
                $json['error'] ="An email with a confirmation link has been sent your Register email address!";
            }
            else
            {
                log_message('error', 'The E-Mail Address was not found in our records, please try again');
                $json['response'] = false;
                $json['error'] ='The E-Mail Address was not found in our records, please try again!';
            }
        }
         $this->response($json,200);
    }

    /**
    * 
    * @function name : verifyforgotcode()
    * @description   : Verify Forgot password code
    * @param         : void
    * @return        : void
    *
    */
    public function verifyforgotcode_post()
    {
        log_message('error', 'in verifyforgotcode_post function');
        $json =array();
        $validation = array(
                             array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|valid_email|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','valid_email' => 'Please enter valid %s')
                                    ),
                             array(
                                'field' => 'forgot_code',
                                'label' => 'Code', 
                                'rules' => 'trim|required|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!')
                                    ),
                            );
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();
            $error_list = implode(" | ", $errors);
            log_message('error', "Check for this errors :- ".$error_list);

            $json['response'] = false;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
        }
        else
        {
            log_message('error', 'validation true');
            $json['response'] = true;

            $email   = $this->post('email');
            $code    = $this->post('forgot_code');

            $data = array(
                'email'     => $email,
                'code'      => $code,
                );

            $res = $this->securitymodel->VerifyForgotPasswordCode($data)->row_array();
            if(!empty($res))
            {
                log_message('error', 'result is not empty');
                if(!empty($res['userimage']))
                {
                    log_message('error', 'userimage is not empty');
                    $path = FCPATH."uploads/profile/".$res['userimage'];
                    if(file_exists($path) && !empty($res['userimage']))
                    {
                        log_message('error', 'userimage is not empty and getting imagepath');
                        $res['userimage'] = base_url('uploads/profile/'.$res['userimage']);
                    }
                    else
                    {
                        log_message('error', 'userimage is empty and getting default image path');
                        $res['userimage'] = base_url('assets/themes/default/images/default_image/male.png');
                    }

                }

                log_message('error', 'success You have modified User');
                $json['response']   = true;
                $json['error']    = "Success: You have modified User!";
                $json['user_info']  = $res;      
            }
            else
            {
                log_message('error', 'Something went wrong');
                $json['response']   = false;
                $json['error']      ='Something went wrong!';
            }
        }
        $this->response($json,200);
    }

    /**
    * 
    * @function name : doforgotpassword()
    * @description   : reset user password
    * @param         : void
    * @return        : void
    *
    */
    public function doforgotpassword_post()
    {
        log_message('error', 'in doforgotpassword_post function');
        $json =array();
        $validation = array(
                            array(
                                    'field' => 'email',
                                    'label' => 'Id', 
                                    'rules' => 'trim|required|xss_clean', 
                                    'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                            array(
                                    'field' => 'forgot_code',
                                    'label' => 'Code', 
                                    'rules' => 'trim|required|xss_clean', 
                                    'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                            array(
                                    'field' => 'new_password',
                                    'label' => 'Password', 
                                    'rules' => 'trim|required|min_length[2]|max_length[255]|xss_clean', 
                                    'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                            array(
                                    'field' => 'confirn_new_password',
                                    'label' => 'Confirm password', 
                                    'rules' => 'trim|required|min_length[2]|max_length[255]|matches[new_password]|xss_clean', 
                                    'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!','matches' => 'Your password and confirm password is not match')
                                    ),
                            );
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();
            $error_list = implode(" | ", $errors);
            log_message('error', "Check for this errors :- ".$error_list);
            
            $json['response']           = false;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning']            = "Warning: Please check the form carefully for errors!";
        }  
        else
        {
            log_message('error', 'validation true');
            $json['response'] = true;

            $email                  = $this->post('email');
            $code                   = $this->post('forgot_code');
            $new_password           = $this->post('new_password');
            $confirm_password       = $this->post('confirn_new_password');

            $data = array(
                'email'             => $email,
                'code'              => $code,
                'new_password'      => $new_password,
                'confirm_password'  => $confirm_password,
                );

            $row = $this->securitymodel->VerifyForgotPasswordCode($data)->num_rows();
            if($row)
            {
                log_message('error', 'row is not empty');
                $res = $this->securitymodel->UpdatePassword($data);
                if($res)
                {   
                    log_message('error', 'You have modified User');
                    $this->securitymodel->UpdateForgotCode($email);
                    $json['response']   = true;
                    $json['error']    = "Success: You have modified User!";     
                }
                else
                {
                    log_message('error', 'Something went wrong');
                    $json['response']   = false;
                    $json['error']      ='Something went wrong!';
                }              
            }
            else
            {
                log_message('error', 'Something went wrong');
                $json['response']   = false;
                $json['error']      ='Something went wrong!';
            }
        }
        $this->response($json,200);        
    }

    /**
    * 
    * @function name : old_password_check()
    * @description   : Check email id already exists or not
    * @param         : void
    * @return        : void
    * @url           : base_url().api/users/old_password_check
    *
    */
    public function old_password_check()
    {  
        log_message('error', 'in old_password_check function');
        $user_id = $this->post('id');
        $password = $this->post('old_password');

        $result = $this->securitymodel->checkPassword($user_id)->row_array();
        if($result)
        {
            log_message('error', 'getting results');
            if (!password_verify($password, $result['password'])) 
            {
                log_message('error', 'old_password_check function return false');
                return false;         
            }           
            else
            {
                log_message('error', 'old_password_check function return true');
                return true;
            }
        } 
    }

    /**
    * 
    * @function name : forgot_verify()
    * @description   : Verify function check inputted email and send link.
    * @param         : void
    * @return        : void
    *
    */
    public function forgot_verify_post()
    {
        log_message('error', 'in forgot_verify_post function');
        $email    = $this->input->post('email');
        $result   = $this->securitymodel->check_email($email);
        $userdata = $result->row_array();
        $username = $userdata['first_name']." ".$userdata['last_name'];

        if(empty($email))
        {
            log_message('error', 'email cannot be empty');
            $json['response'] = true;
            $json['error'] ="email cannot be empty";
        }
        else if($result->num_rows() > 0)
        {
            log_message('error', 'result is not empty');
            $reset_link  =  md5($email.time());
            $set_forgot  =  $this->securitymodel->update_forgot_password_field($email,$reset_link) ;
            $mail_result =  $this->send_reset_link($username,$email,$reset_link) ;
            if($mail_result)
            {   
                log_message('error', 'mail result is not empty');
                $session_data = array(
                    'reset_email_id' => $email,
                );
                $this->session->set_userdata($session_data);
                log_message('error', 'An email with a confirmation link has been sent your Register email address');
                $json['response'] = true;
                $json['error'] ="An email with a confirmation link has been sent your Register email address!";
            }
            else
            {
                log_message('error', 'Email send failed');
                $json['response'] = false;
                $json['error'] ='Email send failed';
            }
        }
        else
        {
            log_message('error', 'The E-Mail Address was not found in our records, please try again');
            $json['response'] = true;
            $json['error'] ='The E-Mail Address was not found in our records, please try again';
        }
        $this->response($json,200);
    }

    /**
    * 
    * @function name : send_reset_link()
    * @description   : send_reset_link function send link to email.
    * @param         : email, reset_link
    * @return        : void
    *
    */
    function send_reset_link($username,$email,$reset_link)
    {
        log_message('error', 'in send_reset_link function');
        $template_code = 'forgot_password';
        $TPL           = $this->securitymodel->getEmailTemplateByCode($template_code);
        $Template      = $this->mailer->Tpl_Email($TPL['template_code'],$email);
        $Recipient     = $email;
        $Filter        = array(
                            '{{NAME}}'       => $username,
                            '{{RESET_LINK}}' => base_url('reset_password_link/'.$reset_link.'/is_mob'),
                            );
        $sent_result   = $this->mailer->Send_Singal_Html_Email($Recipient,$Template,$Filter);

        if($sent_result)
        {
           log_message('error', 'send_reset_link function return true');
           return true ;
        }
        else
        {
            log_message('error', 'send_reset_link function return false');
            return false ;
        }
    }
}
?>