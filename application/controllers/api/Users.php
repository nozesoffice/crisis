<?php
require(APPPATH.'libraries/REST_Controller.php');

// use namespace
//use Restserver\Libraries\REST_Controller;
 
class Users extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Users_model', 'users');
    }
    /**
    * 
    * @function name : user_get()
    * @description   : Retrive users records by user id
    * @access        : public
    * @Method        : get
    * url 			 : http://localhost/ci-rbac-demo/api/users/user
    * @return        : response
    *
    */
    function user_get()
    {
        $json=array();
        if(!$this->get('id'))
        {
            $this->response(NULL, 400);
        }
        $user = $this->users->getUserById((int)$this->get('id'));
        if($user)
        {
            $json['response'] = TRUE;
            $json['success'] = "User data retrive successfully!";
            $json['records'] = $user;
            $this->response($json, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->set_response([
                    'response' => FALSE,
                    'error' => 'User could not be found'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    /**
    * 
    * @function name : listUser_get()
    * @description   : Retrive list of users records
    * @access        : public
    * @Method        : get
    * url            : http://localhost/ci-rbac-demo/api/users/listuser
    * @return        : response
    *
    */
    function listUser_get()
    {
        $json=array();
      
        $users = $this->users->getAllUser();
        if($users)
        {
            $json['response'] = TRUE;
            $json['success'] = "User data retrive successfully!";
            $json['records'] = $users;
            $this->response($json, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->set_response([
                    'response' => FALSE,
                    'error' => 'User could not be found'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    /**
    * 
    * @function name : addUser_post()
    * @description   : Add customer record in database
    * @access        : public
    * @Method        : post
    * @url           : http://localhost/ci-rbac-demo/api/users/adduser
    * @return        : response
    *
    */
    function addUser_post()
    {   
        // print_r($_FILES);
        // exit;
        $json =array();
        $validation = array(
                             array(
                                'field' => 'firstname',
                                'label' => 'First Name', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                             array(
                                'field' => 'lastname',
                                'label' => 'Last Name', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                               array(
                                'field' => 'username',
                                'label' => 'Username', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|callback_check_exits_username|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!','check_exits_username'=>'Username is already exists')
                                    ),
                              array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|valid_email|callback_check_exits_email|xss_clean', 
                                'errors' => array('required' => '%s is required!','check_exits_email'=>'%s is already exists')
                                    ),
                              array(
                                'field' => 'phone',
                                'label' => 'Telephone Number', 
                                'rules' => 'trim|required|is_natural|xss_clean', 
                                'errors' => array('required' => '%s is required!','is_natural' =>"Please provide proper %s")
                                    ),
                              array(
                                'field' => 'password',
                                'label' => 'Password', 
                                'rules' => 'trim|required|min_length[6]', 
                                'errors' => array('required' => '%s is required!')
                                    ),
                              array(
                                'field' => 'userfile',
                                'lable' => 'Image',
                                'rules' => 'callback_file_check'
                                ),
                            );
                    
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $json['response'] = FALSE;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
        }
        else
        {
            $json['response'] = TRUE;

            if($_FILES['userfile']['name'])
            {
                $profile_img = $this->do_upload();
            }
            else
            {
                $profile_img = '';
            }

            $firstname   = $this->post('firstname');
            $lastname    = $this->post('lastname');
            $telephone   = $this->post('telephone');
            $email       = $this->post('email');
            $password    = $this->post('password');
            $role_id     = $this->post('role_id');
            $username    = $this->post('username');

            $data = array(
                'firstname'  	=> $firstname,
                'lastname'   	=> $lastname,
                'phone'  		=> $telephone,
                'email'      	=> $email,
                'password'   	=> $password,
                'role_id'		=> $role_id,
                'username'      => $username,
                'userimage'     => $profile_img,
                );
           	$res = $this->users->addUserData($data);
            if($res)
            {
                $json['success'] ="Success: You have added new User!";     
            }
            else
            {
                $json['response'] = FALSE;
                $json['error'] ='Something went wrong!';
            }
        }
        $this->response($json,200);
    }

     /**
    * 
    * @function name : file_check()
    * @description   : It is for checking file extension
    * @param         : void
    * @return        : boolean
    *
    */

    public function file_check($str)
    {
        $allowed_mime_type_arr = array('image/gif','image/jpeg','image/pjpeg','image/png','image/x-png');
        $mime = get_mime_by_extension($_FILES['userfile']['name']);

        if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['name']!="")
        {
            if(in_array($mime, $allowed_mime_type_arr))
            {
                if($_FILES['userfile']['size'] > 2097152)
                {
                    $this->form_validation->set_message('file_check', 'Please select file with maximum size of 2Mb.');
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                $this->form_validation->set_message('file_check', 'Please select only image file.');
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    /**
    * 
    * @function name : do_upload()
    * @description   : private function for uploading image.
    * @param         : void
    * @return        : void
    *
    */

    private function do_upload()
    {
        $config['upload_path']          = './uploads/profile/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2048;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        $config['overwrite']            = FALSE;
        $config['file_name']            = 'profile'.date("YmdHi").'.png';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('userfile'))
        {
            $file_size = $this->upload->data('file_size');
            if($file_size > 2048)
            {
                $error = "You cannot upload file more than 2MB size";
            }
            else
            {
                $error = $this->upload->display_errors();
            }
            
            $this->session->set_flashdata('error',$error);
            redirect('admin/profile') ;
        }
        else
        {
            return $this->upload->data('file_name');
        }
    }

    /**
    * 
    * @function name : updateUser_post()
    * @description   : edit user record in database
    * @access        : public
    * @Method        : post
    * @url           : http://localhost/ci-rbac-demo/api/users/updateuser
    * @return        : response
    *
    */
     function updateUser_post()
     {
        $json =array();
        $validation = array(
                             array(
                                'field' => 'firstname',
                                'label' => 'First Name', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                             array(
                                'field' => 'lastname',
                                'label' => 'Last Name', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                              array(
                                'field' => 'username',
                                'label' => 'Username', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|callback_check_exits_username|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!','check_exits_username'=>'Username is already exists')
                                    ),
                              array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|valid_email|callback_check_exits_email|xss_clean', 
                                'errors' => array('required' => '%s is required!','check_exits_email'=>'%s is already exists')
                                    ),
                              array(
                                'field' => 'phone',
                                'label' => 'Telephone Number', 
                                'rules' => 'trim|required|is_natural|xss_clean', 
                                'errors' => array('required' => '%s is required!','is_natural' =>"Please provide proper %s")
                                    ),
                              array(
                                'field' => 'id',
                                'label' => 'User Id', 
                                'rules' => 'trim|required|integer|xss_clean', 
                                'errors' => array('required' => '%s is required!')
                                ),
                              array(
                                'field' => 'userfile',
                                'lable' => 'Image',
                                'rules' => 'callback_file_check'
                                ),

                            );
                    
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $json['response'] = FALSE;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
            $this->response($json,404);
        }
        else
        {
            $json['response']   = TRUE;
            
            if($_FILES['userfile']['name'])
            {
                $profile_img = $this->do_upload();
            }
            else
            {
                $profile_img = '';
            }

            $firstname          = $this->post('firstname');
            $lastname           = $this->post('lastname');
            $phone              = $this->post('phone');
            $user_id            = $this->post('id');
            $email              = $this->post('email');
            $username           = $this->post('username');

            $data = array(
                'firstname'     => $firstname,
                'lastname'      => $lastname,
                'phone'         => $phone,
                'email'         => $email,
                'user_id'       => $user_id,
                'username'      => $username,
                'userimage'     => $profile_img,
                );
            $res = $this->users->updateUserData($data);
            if($res)
            {
                $json['success'] ="Success: You have modified User!";     
            }
            else
            {
                $json['response'] = FALSE;
                $json['error'] ='Something went wrong!';
            }
           $this->response($json,200);
        } 
    }

    /**
    * 
    * @function name : deleteUser_delete()
    * @description   : delete user record in database
    * @access        : public
    * @Method        : post
    * @url           : http://localhost/ci-rbac-demo/api/users/deleteuser
    * @return        : response
    *
    */
    function deleteUser_delete($id)
    {   
        $json=array();
        if($id)
        {
            $res = $this->users->deleteUserData($id);
            if($res)
            {
                $json['response'] = TRUE;
                $json['success'] ="Success: You have deleted User!"; 
            }
            else
            {
                $json['response'] = FALSE;
                $json['error'] ='Something went wrong!';
            }
            $this->response($json,200);
        }
    }

    /**
    * 
    * @function name : registration_post()
    * @description   : New user registration
    * @access        : public
    * @Method        : post
    * @url           : http://localhost/ci-rbac-demo/api/users/registration
    * @return        : response
    *
    */
    function registration_post()
    {
        $json =array();
        $validation = array(
                             array(
                                'field' => 'firstname',
                                'label' => 'First Name', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                             array(
                                'field' => 'lastname',
                                'label' => 'Last Name', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                               array(
                                'field' => 'username',
                                'label' => 'Username', 
                                'rules' => 'trim|required|min_length[2]|max_length[255]|callback_check_exits_username|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!','check_exits_username'=>'Username is already exists')
                                    ),
                              array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|valid_email|callback_check_exits_email|xss_clean', 
                                'errors' => array('required' => '%s is required!','check_exits_email'=>'%s is already exists')
                                    ),
                              array(
                                'field' => 'phone',
                                'label' => 'Telephone Number', 
                                'rules' => 'trim|required|is_natural|xss_clean', 
                                'errors' => array('required' => '%s is required!','is_natural' =>"Please provide proper %s")
                                    ),
                              array(
                                'field' => 'password',
                                'label' => 'Password', 
                                'rules' => 'trim|required|min_length[6]', 
                                'errors' => array('required' => '%s is required!')
                                    ),
                              array(
                                'field' => 'cpassword',
                                'label' => 'Confirm password', 
                                'rules' => 'trim|required|min_length[6]|matches[password]', 
                                'errors' => array('required' => '%s is required!','matches' => 'Password and confirm password are not match')
                                    ),
                            );
                    
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $json['response'] = FALSE;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
        }
        else
        {
            $json['response'] = TRUE;

            $firstname   = $this->post('firstname');
            $lastname    = $this->post('lastname');
            $telephone   = $this->post('telephone');
            $email       = $this->post('email');
            $password    = $this->post('password');
            $username    = $this->post('username');

            $data = array(
                'firstname'     => $firstname,
                'lastname'      => $lastname,
                'phone'         => $telephone,
                'email'         => $email,
                'password'      => $password,
                'role_id'       => 2,
                'username'      => $username,
             
                );
            $res = $this->users->Register($data);
            if($res)
            {
                $json['success'] ="Success: Your registration process has been done successfully and you have to receive email with account activation link into your register email address so please check it and verify your account";     
            }
            else
            {
                $json['response'] = FALSE;
                $json['error'] ='Something went wrong!';
            }
        }
        $this->response($json,200);
    }



    /**
    * 
    * @function name : loginUser_post
    * @description   : login customer
    * @access        : public
    * @HTTP Method   : post
    * @return        : json
    * @url           : customer/customer/loginUser
    */
    function loginUser_post()
    {
        $json =array();
        $validation = array(
                            array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|valid_email|xss_clean', 
                                'errors' => array('required' => '%s is required!','check_exits_email'=>'%s is already exists')
                                ),
                            array(
                                'field' => 'password',
                                'label' => 'Password', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => '%s is required!')
                                ),

                            );
                    
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $json['response'] = FALSE;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
        }
        else
        {

            $json['response'] = TRUE;
            
            $email       = $this->post('email');
            $password    = $this->post('password');

            $data = array(
                'email'      => $email,
                'password'   => $password,
                );

            $result = $this->users->Login($data);
            if($result)
            {
                if (!password_verify($password, $result['password'])) 
                {
                    $json['response'] = FALSE;
                    $json['error'] ='Invalid email/password!';          
                }           
                else if($result['active'] == 0)
                {
                    $json['response'] = FALSE;
                    $json['error'] ='Your account is not actived yet, please active your account by verifying your email address!';
                }
                else
                {
                    // Set session data
                    $session_data = array(
                        'user_id'           => $result['id'],
                        'user_name'         => $result['first_name'].' '.$result['last_name'],
                        'user_email'        => $result['email'],
                        'user_status'       => $result['active'],
                        'user_login_status' => TRUE,
                        'session_id'        => session_id()
                    );
                    $this->session->set_userdata($session_data) ;
                    $json['success'] ="Success: you have been successfully logged in!";
                    $json['data'] = $result;
                }   
            }
            else
            {
                $json['response'] = FALSE;
                $json['error'] ='Invalid email/password!';
            }

        }
        $this->response($json,200);
    }

     /**
    * 
    * @function name : forgotPassword_post
    * @description   : forgot password request by customer
    * @access        : public
    * @HTTP Method   : post
    * @return        : json
    * @url           : customer/customer/loginUser
    */
    function forgotPassword_post()
    {
        $json =array();
        $validation = array(
                            array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|valid_email|xss_clean', 
                                'errors' => array('required' => '%s is required!')
                            ),
                        );
                    
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $json['response'] = FALSE;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
        }
        else
        { 
            $email       = $this->post('email');
            $data = array(
                'email'      => $email,
                );

            $result = $this->users->forgotPassword($data);
            if($result)
            {   
                $session_data = array(
                    'reset_email_id' => $email,
                );
                $this->session->set_userdata($session_data);
                $json['response'] = "SUCCESS";
                $json['success'] ="An email with a confirmation link has been sent your Register email address!";
            }
            else
            {
                $json['response'] = "FAIELD";
                $json['error'] ='The E-Mail Address was not found in our records, please try again!';
            }
        }
         $this->response($json,200);
    }

    /**
    * 
    * @function name : changePassword_post
    * @description   : change password request by customer
    * @access        : public
    * @HTTP Method   : post
    * @return        : json
    * @url           : customer/customer/changePassword
    */
   function changePassword_post()
   {
        $validation = array(
                    array(
                        'field' => 'old_password',
                        'label' => 'Old Password', 
                        'rules' => 'required|callback_old_password_check', 
                        'errors' => array('required' => '%s must be required!','old_password_check'=>'%s is wrong!')
                    ),
        
                    array(
                        'field' => 'new_password',
                        'label' => 'Password', 
                        'rules' => 'required|min_length[6]|max_length[32]', 
                        'errors' => array('required' => '%s must be between 6 and 32 characters!','min_length'=>'%s must be between 6 and 32 characters!','max_length'=>'%s must be between 6 and 32 characters!')
                    ),
               
                    array(
                        'field' => 'confirm_password',
                        'label' => 'Confirm Password', 
                        'rules' => 'trim|matches[new_password]', 
                        'errors' => array('matches'=>'New Password and password confirmation do not match!')
                    ),
                     array(
                        'field' => 'id',
                        'label' => 'User Id', 
                        'rules' => 'trim|required|xss_clean', 
                        'errors' => array('required' => '%s is required!')
                            ),
                );
                $this->form_validation->set_rules($validation);
                if ($this->form_validation->run() == FALSE) 
                {
                    $json['response'] = FALSE;
                    $json['validation_message'] = $this->form_validation->error_array();
                    $json['warning'] = "Warning: Please check the form carefully for errors!";
                }
                else
                {
                    $password   = $this->post('new_password');
                    $user_id    = $this->post('id');
                    $data       = array(
                                    'password'      => $password,
                                    'user_id'       => $user_id
                    );
                    $result = $this->users->editPassword($data);
                    if($result)
                    {
                        $json['response'] = TRUE;
                        $json['success'] ="Your password is changed!";
                            
                    }
                    else
                    {
                        $json['response'] = FALSE;
                        $json['error'] ='Something went wrong!';
                    }
                }
                $this->response($json,200);
            }

    /**
    * 
    * @function name : old_password_check()
    * @description   : Check email id already exists or not
    * @param         : void
    * @return        : void
    *
    */
    public function old_password_check()
    {  
        $user_id = $this->post('id');
        $password = $this->post('old_password');

        $result = $this->users->checkPassword($user_id)->row_array();
        if($result)
        {
            if (!password_verify($password, $result['password'])) 
            {
                return FALSE;         
            }           
            else
            {
                return TRUE;
            }
        } 
    }

    /**
    * 
    * @function name : verifyforgotcode()
    * @description   : Verify Forgot password code
    * @param         : void
    * @return        : void
    *
    */
    public function verifyforgotcode_post()
    {
        $json =array();
        $validation = array(
                             array(
                                'field' => 'email',
                                'label' => 'Email', 
                                'rules' => 'trim|required|valid_email|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!','valid_email' => 'Please enter valid %s')
                                    ),
                             array(
                                'field' => 'forgot_code',
                                'label' => 'Code', 
                                'rules' => 'trim|required|xss_clean', 
                                'errors' => array('required' => '%s must be between 2 and 255 characters!')
                                    ),
                            );
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $json['response'] = FALSE;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning'] = "Warning: Please check the form carefully for errors!";
        }
        else
        {
            $json['response'] = TRUE;

            $email   = $this->post('email');
            $code    = $this->post('forgot_code');

            $data = array(
                'email'     => $email,
                'code'      => $code,
                );

            $res = $this->users->VerifyForgotPasswordCode($data)->row_array();
            if(!empty($res))
            {
                $json['response']   = TRUE;
                $json['success']    = "Success: You have modified User!";
                $json['user_info']  = $res;      
            }
            else
            {
                $json['response']   = FALSE;
                $json['error']      ='Something went wrong!';
            }
        }
        $this->response($json,200);
    }

    /**
    * 
    * @function name : doforgotpassword()
    * @description   : reset user password
    * @param         : void
    * @return        : void
    *
    */
    public function doforgotpassword_post()
    {
        $json =array();
        $validation = array(
                            array(
                                    'field' => 'email',
                                    'label' => 'Id', 
                                    'rules' => 'trim|required|xss_clean', 
                                    'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                            array(
                                    'field' => 'forgot_code',
                                    'label' => 'Code', 
                                    'rules' => 'trim|required|xss_clean', 
                                    'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                            array(
                                    'field' => 'new_password',
                                    'label' => 'Password', 
                                    'rules' => 'trim|required|min_length[2]|max_length[255]|xss_clean', 
                                    'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!')
                                    ),
                            array(
                                    'field' => 'confirn_new_password',
                                    'label' => 'Confirm password', 
                                    'rules' => 'trim|required|min_length[2]|max_length[255]|matches[new_password]|xss_clean', 
                                    'errors' => array('required' => '%s must be between 2 and 255 characters!','min_length'=>'%s must be between 2 and 255 characters!','max_length'=>'%s must be between 2 and 255 characters!','matches' => 'Your password and confirm password is not match')
                                    ),
                            );
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) 
        {
            $json['response']           = FALSE;
            $json['validation_message'] = $this->form_validation->error_array();
            $json['warning']            = "Warning: Please check the form carefully for errors!";
        }  
        else
        {
            $json['response'] = TRUE;

            $email                  = $this->post('email');
            $code                   = $this->post('forgot_code');
            $new_password           = $this->post('new_password');
            $confirm_password       = $this->post('confirn_new_password');

            $data = array(
                'email'             => $email,
                'code'              => $code,
                'new_password'      => $new_password,
                'confirm_password'  => $confirm_password,
                );

            $row = $this->users->VerifyForgotPasswordCode($data)->num_rows();
            if($row)
            {
                $res = $this->users->UpdatePassword($data);
                if($res)
                {   
                    $this->users->UpdateForgotCode($email);
                    $json['response']   = TRUE;
                    $json['success']    = "Success: You have modified User!";     
                }
                else
                {
                    $json['response']   = FALSE;
                    $json['error']      ='Something went wrong!';
                }              
            }
            else
            {
                $json['response']   = FALSE;
                $json['error']      ='Something went wrong!';
            }
        }
        $this->response($json,200);        
    }

    /**
    * 
    * @function name : check_exits_email
    * @description   : Check exiting email in customer data
    * @param1        : string
    * @return        : void
    *
    */
    function check_exits_email($str)
    {
            $this->db->from('tbl_users');
            $this->db->where('email',$this->post('email'));
            if($this->post('id') !=="")
            {
              $this->db->where('id !=',(int)$this->post('id'));  
            }
            $query=$this->db->get();
            $row = $query->num_rows();
            if($row > 0)
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }   
    }

    /**
    * 
    * @function name : check_exits_username
    * @description   : Check exiting email in customer data
    * @param1        : string
    * @return        : void
    *
    */
    function check_exits_username($str)
    {
            $this->db->from('tbl_users');
            $this->db->where('username',$this->post('username'));
            if($this->post('id') !=="")
            {
              $this->db->where('id !=',(int)$this->post('id'));  
            }
            $query=$this->db->get();
            $row = $query->num_rows();
            if($row > 0)
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }   
    }
}
?>