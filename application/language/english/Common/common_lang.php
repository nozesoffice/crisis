<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		common_lang
*	@Auther:		Rohit Singh
*	@Date:			27-10-2017
*	@Description:	Manage Common text and messages.
*
*/

/*::::: Common :::::*/

//text
$lang['add_button_text']               = 'Add';
$lang['update_button_text']            = 'Update';

//Error msg

/*::::: Common :::::*/