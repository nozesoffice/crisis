<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		auth_lang
*	@Auther:		Rohit Singh
*	@Date:			27-10-2017
*	@Description:	Manage Login,Forgot and Reset password text and messages.
*
*/

/*::::: Login :::::*/

//text
$lang['login_title']                      = 'Admin Login';
$lang['login_heading']                    = 'Sign in to Admin Panel';
$lang['login_email_placeholder']          = 'Username or email';
$lang['login_password_placeholder']       = 'Password';
$lang['login_forgot_text']                = 'Forgot Password ?';
$lang['login_button_text']                = 'Sign In';

//Error msg
$lang['login_invalid_msg']                = 'Invalid Email or Password.';
$lang['account_deactivated_msg']          = 'Account is Deactivated.';

/*::::: Login :::::*/

/*::::: Forgot :::::*/
//text
$lang['forgot_title']                     = 'Admin Forgot';
$lang['forgot_heading']                   = 'Forgot Password';
$lang['login_text']                       = 'Login ?';
$lang['reset_button_text']                = 'Send Reset Link';

//Success msg
$lang['email_sent_msg']                   = 'Email has been sent successfully. Please check your email inbox.';

//Error msg
$lang['email_required_msg']               = "Email field can't be empty.";
$lang['email_not_found_msg']              = "Email has not been found.";
$lang['email_failed_msg']                 = "Email delivery failed.";
/*::::: Forgot :::::*/

/*::::: Reset :::::*/
//text
$lang['reset_password_title']             = 'Admin Reset Password';
$lang['reset_password_heading']           = 'Reset Password';
$lang['new_password_placeholder']         = 'New Password';
$lang['confirm_new_password_placeholder'] = 'Confirm New Password';
$lang['update_password_button_text']      = 'Update Password';

//Error msg
$lang['reset_link_expired_msg']		      = 'Reset password link expired.';
$lang['reset_password_required_msg']	  = 'Password field required.';
$lang['reset_password_not_match_msg']	  = 'Confirm Password doesn`t match with Password.';
$lang['password_not_updated_msg']	  	  = 'Password Can`t Updated.';

//Success msg
$lang['password_updated_msg']	      	  = 'Password Updated Successfully.';
/*::::: Reset :::::*/
