<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		changepass_lang
*	@Auther:		Akshay Patel
*	@Date:			02-03-2019
*	@Description:	Manage Change Password in admin language
*
*/

/*::::: Login :::::*/

//text
$lang['changepass_title']     = 'Change Password';
$lang['changepass_heading']     = 'Change Password';
