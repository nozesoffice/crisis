<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		cms_lang
*	@Auther:		Rohit Singh
*	@Date:			10-02-2018
*	@Description:	Manage CMS language in admin 
*
*/

/*::::: CMS :::::*/

//text
$lang['cms_title']       = 'CMS';
$lang['cms_heading']     = 'CMS';
