<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		dashboard_lang
*	@Auther:		Rohit Singh
*	@Date:			28-10-2017
*	@Description:	Manage Dashboard text and messages.
*
*/

/*::::: Dashboard :::::*/

//text
$lang['dashboard_title']             = 'Admin Dashboard';
$lang['dashboard_heading']           = 'Dashboard';

//Error msg

/*::::: Dashboard :::::*/