<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		role_lang
*	@Auther:		Rohit Singh
*	@Date:			10-02-2018
*	@Description:	Manage Role in admin language
*
*/

/*::::: Login :::::*/

//text
$lang['role_title']       = 'Roles';
$lang['role_heading']     = 'Roles';
