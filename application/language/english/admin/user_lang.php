<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
*	@Filename: 		user_lang
*	@Auther:		Rohit Singh
*	@Date:			10-02-2018
*	@Description:	Manage User in admin language
*
*/

/*::::: Login :::::*/

//text
$lang['user_title']       = 'Users';
$lang['user_heading']     = 'Users';
