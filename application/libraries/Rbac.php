<?php
class RBAC 
{	
	private $ModuleAccess;
	function __construct()
	{
		$this->obj =& get_instance();
		$this->obj->ModuleAccess = $this->obj->session->userdata('ModuleAccess');
	}
	//-------------------------------------------------------------
	function CheckAuthentication()
	{
		if(!$this->obj->session->userdata('logged_in'))
		{
			$Back_To =str_replace("truedeals/","",$_SERVER['REQUEST_URI']);
			$Back_To = $this->obj->functions->encode($Back_To);
			redirect('login/'.$Back_To);
		}
	}
	//----------------------------------------------------------------
	function SetAccessInSession()
	{
		$this->obj->db->from('tbl_group_access');
		$this->obj->db->where('group_id',$this->obj->session->userdata('role_id'));
		$query=$this->obj->db->get();
		$data=array();		
		foreach($query->result_array() as $v)
		{
			$data[$v['module_name']][$v['operation']]='';
		}
		
		$this->obj->session->set_userdata('ModuleAccess',$data);
	} 	
	//--------------------------------------------------------------	
	function CheckModulePermission($Module)
	{
		
		// echo "<pre>";
		// print_r($this->obj->session->userdata());exit;
		if($this->obj->session->userdata('role_id') <=1) 
			return 1;
		if(isset($this->obj->ModuleAccess[$Module])) 
			return 1;
		else 
		 	return 0;
	}
	//--------------------------------------------------------------	
	function CheckOperationPermission($Operation)
	{
		
		if($this->obj->session->userdata('role_id') <=1) 
			return 1;
		if(isset($this->obj->ModuleAccess[$this->obj->uri->segment(1)][$Operation])) 
			return 1;
		else 
		 	return 0;
	}
	//--------------------------------------------------------------	
	function CheckOperationPermissionForMenu($controller,$Operation)
	{
		
		if($this->obj->session->userdata('role_id') <=1) 
			return 1;
		if(isset($this->obj->ModuleAccess[$controller][$Operation])) 
			return 1;
		else 
		 	return 0;
	}
	//--------------------------------------------------------------	
	function CheckModuleAccess()
	{
		//echo $this->obj->uri->segment(1);
		if(!$this->CheckModulePermission($this->obj->uri->segment(1)))
		{
			$Back_To =str_replace("truedeals/","",$_SERVER['REQUEST_URI']);
			$Back_To = $this->obj->functions->encode($Back_To);
			redirect('access_denied/'.$Back_To);
		}
		
	}
	//--------------------------------------------------------------	
	function CheckOperationAccess()
	{
		$operation = $this->obj->uri->segment(2);
		
		//if(!$this->CheckOperationPermission($this->obj->uri->segment(2)))

		if(!$this->CheckOperationPermission($operation))
		
		{
			$Back_To =str_replace("truedeals/","",$_SERVER['REQUEST_URI']);
			$Back_To = $this->obj->functions->encode($Back_To);
			redirect('access_denied/'.$Back_To);
		}
	}		
}
?>