<?php
/**
* 
* @file name   : Common
* @Auther      : Hunny
* @Date        : 10-07-2017
* @Description : Common Business Logic
*
*/
class Common extends CI_Model 
{
		
	function __construct()
	{
		parent::__construct();
		
	}

/**
* 
* @function name : config
* @description   : Find string length
* @param1        : string
* @return        : array
*
*/
    function config($key='') {

        $this->db->where('key',$key);
        $query = $this->db->get('tbl_setting');
        $res=$query->row_array();
        if(isset($res['val']) && $res['key'] != 'admin_theme')
        {

        //echo "<pre>";
        //print_r($res);
        //exit;
        }
        return isset($res['val'])?$res['val']:'';

    }	

/**
* 
* @function name : escape
* @description   : This can help prevent SQL injection attacks which are often performed by using the ' character to append malicious code to an SQL query.
* @param1        : string
* @return        : string
*
*/    
    function escape($val) {
        
        return @mysqli_real_escape_string($val);
        
    }

    function GetByKey($k,$v)
    {
        $this->db->from('email_template');
        $this->db->where($k,$v);
        $query=$this->db->get();
        return $query->result_array();
    }

   function GetEmailTemplateDetails()
    {
            
            $query = $this->db->get('email_template');
            return $query->result_array();
    }

    /**
    * 
    * @function name : get_userdata()
    * @description   : Get loggined userdata by email.
    * @param         : email
    * @return        : void
    *
    */
    function get_userdata($email)
    {
        $this->db->join('tbl_roles','tbl_roles.role_id = tbl_users.tbl_roles_role_id','inner');
        $query = $this->db->get_where('tbl_users',array('tbl_users.email'=>$email))->row() ;
        return $query ;
    }
}
?>