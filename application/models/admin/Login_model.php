<?php
/**
* 
* @file name   : Login
* @Auther      : Rohit Singh
* @Date        : 25-10-2017
* @Description : Login detail
*
*/
class Login_model extends CI_Model 
{
	/**
	* 
	* @function name : signin()
	* @description   : check registered email
	* @param   	 	 : email
	* @return        : user data
	*
	*/
    function signin($email)
    {
    	$query = $this->db->where('email',$email)->or_where('username',$email)->get('tbl_users');

		return $query ; 
    }

    /**
	* 
	* @function name : check_email()
	* @description   : check registered email
	* @param   	 	 : email
	* @return        : user data
	*
	*/
	public function check_email($email)
	{
		$query = $this->db->get_where('tbl_users',array('email' => $email));
    	return $query ;
	}

	/**
	* 
	* @function name : update_forgot_password_field()
	* @description   : update forgot password field
	* @param   	 	 : email, reset_link
	* @return        : void
	*
	*/
	public function update_forgot_password_field($email,$reset_link,$password_updated = '')
	{
		$data = array(
		'forgotten_password_code' => $reset_link,
		'forgotten_password_time' => ($password_updated) ? '' : date('YmdHis'),
		) ;

		$this->db->set($data);
		$this->db->where('email', $email)
				 ->update('tbl_users') ;

		if($this->db->affected_rows() > 0)
		{
			return true ;
		}
		else
		{
			return false ;
		}
	}

	/**
	* 
	* @function name : check_reset_link()
	* @description   : check forgot password link
	* @param   	 	 : link
	* @return        : boolean
	*
	*/
	public function check_reset_link($link)
	{
		$data = $this->db->get_where('tbl_users',array('forgotten_password_code'=>$link))->row_array();
		return $data ;
	}

	/**
	* 
	* @function name 	: getEmailTemplateByCode()
	* @description   	: get email_template record by template_code
	* @param   			: $template_code The template_code  that you want
	* @return       	: array The selected template_code array
	*
	*/
	public function getEmailTemplateByCode($template_code)
    {
        $this->db->from('email_template');
        $this->db->where('template_code',$template_code);
        $query=$this->db->get();
        return $query->row_array();
    }

    /**
	* 
	* @function name 	: update_new_password()
	* @description   	: Update password by email
	* @param   			: $new_password,$email
	* @return       	: boolean true or false
	*
	*/
	public function update_new_password($new_password,$email)
    {
        $this->db->set('password',$new_password);
        $this->db->where('email',$email);
        $query=$this->db->update('tbl_users');
        if($query)
        {
        	return true ;
        }
        else
        {
        	return false ;
        }
    }

    /**
	* 
	* @function name 	: update_user_password()
	* @description   	: Update password by email
	* @param   			: $new_password,$email
	* @return       	: boolean true or false
	*
	*/
	public function update_user_password($new_password,$user_id,$code)
    {
        $this->db->set('password',$new_password);
        $this->db->where('forgotten_password_code',$code);
        $this->db->where('id',$user_id);
        $query=$this->db->update('tbl_users');
        if($query)
        {
        	return true ;
        }
        else
        {
        	return false ;
        }
    }
}