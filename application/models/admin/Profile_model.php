<?php
/**
* 
* @file name   : Profile
* @Auther      : Rohit Singh
* @Date        : 25-10-2017
* @Description : Profile Detail
*
*/
class Profile_model extends CI_Model 
{
	/**
	* 
	* @function name : update_profile()
	* @description   : Update user data.
	* @param   	 	 : email,profile_img
	* @return        : Boolean
	*
	*/
	function update_profile($email,$profile_img)
	{
		$data = array(
			'username'    => $this->input->post('user_name'),
			'company'    => $this->input->post('company'), 
			'first_name' => $this->input->post('first_name'), 
			'last_name'  => $this->input->post('last_name'),  
			'phone'      => $this->input->post('contact'), 
			'created_on' => date('Y-m-d H:i:s'), 
		);
		if(!empty($profile_img))
		{
			$data['userimage'] = $profile_img;
		}

		$this->db->where('email',$email);
		$this->db->update('tbl_users',$data);

		if($this->db->affected_rows() > 0)
		{
			return true ;
		}
		else
		{
			return false ;
		}
	}

	/**
	* 
	* @function name : Change_Password()
	* @description   : change account password.
	* @param   	 	 : 
	* @return        : Boolean
	*
	*/
	function Change_Password()
	{	
		$user_data = $this->db->get_where('tbl_users',array('id' =>$this->session->userdata('uid')))->row_array();

		$old_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');

		if(!password_verify($old_password,$user_data['password']))
		{
			return false;
		}
		else
		{
			$this->db->set('password',password_hash($new_password,PASSWORD_DEFAULT));
			$this->db->where("id",$user_data['id']);
			$result = $this->db->update('tbl_users');
			return true;
		}
	}

	/**
	* 
	* @function name : check_unique_user_username()
	* @description   : Check for unique username
	* @param   	 	 : 
	* @return        : Boolean
	*
	*/
	function check_unique_user_username($user_id, $username) 
	{
        $this->db->where('username', $username);
        if($user_id) 
        {
            $this->db->where('id != ', $user_id);
        }
        return $this->db->get('tbl_users')->num_rows();
	}

	/**
	* 
	* @function name : check_unique_user_phone()
	* @description   : Check for unique phone
	* @param   	 	 : 
	* @return        : Boolean
	*
	*/
	function check_unique_user_phone($user_id, $phone) 
	{
        $this->db->where('phone', $phone);
        if($user_id) 
        {
            $this->db->where('id != ', $user_id);
        }
        return $this->db->get('tbl_users')->num_rows();
	}
}