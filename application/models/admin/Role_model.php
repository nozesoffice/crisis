<?php
/**
* 
* @file name   : Role
* @Auther      : Rohit Singh
* @Date        : 12-02-2018
* @Description : Role Detail
*
*/
class Role_model extends CI_Model 
{
	/**
	* 
	* @function name : Save Role
	* @description   : Save Role data.
	* @param   	 	 : 
	* @return        : Array
	*
	*/

	public function saverole()
	{
		$role_data = array(
			'role_name'         => $this->input->post('role_name'),
			'role_description'  => $this->input->post('role_description'),
			'role_status' 	    => $this->input->post('role_status'),
		);

		$result = $this->db->insert('tbl_roles',$role_data);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	* 
	* @function name : Update Roles
	* @description   : Update role data.
	* @param   	 	 : Role data
	* @return        : Boolean
	*
	*/

	function updaterole()
	{
		$role_id = $this->input->post('hidden_role_id');

		$role_data = array(
			'role_name'         => $this->input->post('role_name'),
			'role_description'  => $this->input->post('role_description'),
			'role_status' 	    => $this->input->post('role_status'),
		);		
		$result = $this->db->where('role_id',$role_id)->update('tbl_roles',$role_data);

		return TRUE;		
	}

	function get_role_data($role_id)
	{
		$result = $this->db->get_where("tbl_roles",array("role_id"=>$role_id))->row_array();
		return $result;
	}

	function delete($role_id)
	{
		$data = $this->db->where('role_id',$role_id)->delete("tbl_roles");

		$this->db->where('tbl_roles_role_id',$role_id)->delete("tbl_users");

		if($data)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function get_system_modules()
	{
		$query = $this->db->get('tbl_system_modules');

		return $query->result_array();
	}

	function GetAccess($role_id)
	{
		$this->db->from('tbl_group_access');
		$this->db->where('group_id',$role_id);
		$query=$this->db->get();
		$data=array();
		foreach($query->result_array() as $v)
		{
			$data[]=$v['module_name'].'/'.$v['operation'];
		}
		return $data;
	}

	function SetAccess()
	{
		$this->db->where('group_id',$this->input->post('role_id'));
		$this->db->delete('tbl_group_access');

		$data = array();
		if(count($this->input->post('Access'))==0) return 1;

		foreach($this->input->post('Access') as $Access)
		{
			$ac = explode("/",$Access);
			$data[]=array('group_id' => $this->input->post('role_id') ,
						  'module_name' => $ac[0],
						  'operation'   => $ac[1]);
		}
		return $this->db->insert_batch('tbl_group_access', $data);
	}

//============  STATUS CHANGE SELECTED Function ===================
	function ChangeStatusSelected($status) 
	{
		$this->db->set('role_status', $status);
		$this->db->where_in('role_id', $this->input->post('Id_List'));
		if ($query = $this->db->update('tbl_roles'))
			return true;
		else
			return false;
	}
	
//============  DELETE SELECTED Function ===================
	function DeleteSelected() 
	{	
		$this->db->where_in('role_id', $this->input->post('Id_List'));
		$query = $this->db->delete('tbl_roles');
		return true;
	}     


}