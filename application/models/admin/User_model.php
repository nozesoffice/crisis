<?php
/**
* 
* @file name   : Profile
* @Auther      : Rohit Singh
* @Date        : 25-10-2017
* @Description : Profile Detail
*
*/
class User_model extends CI_Model 
{
	/**
	* 
	* @function name : all_users
	* @description   : all user data.
	* @param   	 	 : 
	* @return        : Array
	*
	*/

	function all_users()
	{
		$result = $this->db->where('tbl_roles.role_id != 1')
						   ->join('tbl_roles','tbl_roles.role_id = tbl_users.tbl_roles_role_id',"inner")
						   ->get("tbl_users")
						   ->result_array();
		return $result;
	}


	/**
	* 
	* @function name : Save Users
	* @description   : save user data.
	* @param   	 	 : Users data
	* @return        : Boolean
	*
	*/

	function saveuser()
	{
		$user_data = array(
			'first_name'        => $this->input->post('first_name'),
			'last_name'         => $this->input->post('last_name'),
			'email'             => $this->input->post('email'),
			'username'          => $this->input->post('user_name'),
			'phone'             => $this->input->post('phone'),
			'password'          => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'active' 	        => 1,
			'tbl_roles_role_id' => $this->input->post('user_role'),
		);

		$result = $this->db->insert('tbl_users',$user_data);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	* 
	* @function name : Update Users
	* @description   : Update user data.
	* @param   	 	 : Users data
	* @return        : Boolean
	*
	*/

	function updateuser()
	{
		$user_id = $this->input->post('hidden_user_id');
		$data = array(
			'first_name'        => $this->input->post('first_name'),
			'last_name'         => $this->input->post('last_name'),
			'username'          => $this->input->post('user_name'),
			'phone'             => $this->input->post('phone'),
			'email'             => $this->input->post('email'),
			'active' 	        => $this->input->post('user_status'),
			'tbl_roles_role_id' => $this->input->post('user_role'),
		);
		if ($this->input->post('password'))
		{
			$data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
		}
		$result = $this->db->where('id',$user_id)->update('tbl_users',$data);
		return TRUE;
		
	}

	function get_user_data($user_id)
	{
		$result = $this->db->select('tbl_users.id as user_id,tbl_roles.*,tbl_users.*')->join('tbl_roles','tbl_roles.role_id = tbl_users.tbl_roles_role_id',
			'inner')->get_where("tbl_users",array("id"=>$user_id))->row_array();
		return $result;
	}

	function delete($user_id)
	{
		$this->db->where('id',$user_id)->delete("tbl_users");
		if($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

//============  STATUS CHANGE SELECTED Function ===================
	function ChangeStatusSelected($status) 
	{
		$this->db->set('active', $status);
		$this->db->where_in('id', $this->input->post('Id_List'));
		if ($query = $this->db->update('tbl_users'))
			return true;
		else
			return false;
	}
	

//============  DELETE SELECTED Function ===================
	function DeleteSelected() 
	{	
		$this->db->where_in('id', $this->input->post('Id_List'));
		$query = $this->db->delete('tbl_users');
		return true;
	}   
	
//============  Check username is unique or not ===================	
	function check_unique_user_username($user_id, $username) 
	{
        $this->db->where('username', $username);
        if($user_id) 
        {
            $this->db->where('id != ', $user_id);
        }
        return $this->db->get('tbl_users')->num_rows();
	}

//============  Check email is unique or not ===================	
	function check_unique_user_email($user_id, $email) 
	{
        $this->db->where('email', $email);
        if($user_id) 
        {
            $this->db->where('id != ', $user_id);
        }
        return $this->db->get('tbl_users')->num_rows();
	}

//============  Check email is unique or not ===================	
	function check_unique_user_phone($user_id, $phone) 
	{
        $this->db->where('phone', $phone);
        if($user_id) 
        {
            $this->db->where('id != ', $user_id);
        }
        return $this->db->get('tbl_users')->num_rows();
	}
}