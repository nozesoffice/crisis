<?php
/**
* 
* @file name   : Security_model
* @Auther      : Dhaval Balar
* @Date        : 20-02-2019
* @Description : Collection of various common function related to user database operation.
*
*/
class Security_model extends CI_Model 
{
    /**
    * 
    * @function name 	: __construct()
    * @description   	: initialize variables
    * @param   		    : void
    * @return        	: void
    *
    */
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('mailer');
       
    }

    /**
    * 
    * @function name : Register()
    * @description   : New user registration
    * @access        : public
    * @return        : void
    *
    */
    public function Register($data=array()) 
    {
        log_message('error', 'in Register function');
        if($data)
        {
            log_message('error', 'data is set');
            $activation_code = $this->commons->generate_randomnumber(6);
            
            //$this->db->set('first_name', $data['firstname']);
            //$this->db->set('last_name', $data['lastname']);
            $this->db->set('username',$data['username']);
            $this->db->set('email', $data['email']);
            $this->db->set('userimage',$data['userfile']);
            //$this->db->set('phone', $data['phone']);
            $this->db->set('tbl_roles_role_id',$data['role_id']);
            $this->db->set('ip_address', $_SERVER['REMOTE_ADDR']);
            //$this->db->set('activation_code',$activation_code);
            $this->db->set('password', password_hash($data['password'], PASSWORD_DEFAULT));
            $this->db->set('active', 1);
            $this->db->set('created_on',date('Y-m-d h:i:sa'));
            $this->db->insert('tbl_users');
            $user_id = $this->db->insert_id();
            
            // if($user_id > 0) 
            // {
            //     $name = $data['firstname']." ".$data['lastname'];
            //     $email = $data['email'];
            //     $activation_link = base_url('account/register/active_user/').$this->commons->encode($user_id).'/'.$this->commons->encode($activation_code);
                
            //     //=== Send Email ====//
            //     $Template = $this->mailer->Tpl_Email('user_signup',$this->commons->encode($email));
            //         $Recipient = $email;
            //         $Filter = array(
            //             '{{NAME}}' =>$name,
            //             '{{ACTIVATION_LINK}}' => $activation_link,
            //         );
            //     $this->mailer->Send_Singal_Html_Email($Recipient,$Template,$Filter);
            //     //===================
            // } 
            return $user_id;
            log_message('error', 'return last inserted user id');
        }
        else
        {
            log_message('error', 'return false');
            return FALSE;
        }
    }


    /**
    * 
    * @function name    : Login()
    * @description      : Check UserName and Password for Aithantication
    * @access           : public
    * @param            : void
    * @return           : void
    *
    */
    public function Login($data=array())    
    { 
        log_message('error', 'in Login function');
        if($data)
        {
            log_message('error', 'data is set');
            $this->db->select('id,email,active,username,password,userimage');
            $this->db->from('tbl_users');
            $this->db->where('email',$data['email']);
            $query=$this->db->get();
                   
            if ($query->num_rows() == 0)  return FALSE; log_message('error', 'invalid is or password'); // invalid id or password
                $result = $query->row_array();
            
            if ($result['active'] == 0)  return $result; log_message('error', 'Inactive Account'); // Inactive Account
            $token = array();
            $token['id'] = $result['id'];
            $this->db->set('last_login',date('Y-m-d h:i:sa'))->where('id',$result['id'])->update('tbl_users');    
            return $result;
            log_message('error', 'return result');
        }         
    }
    

    /** 
    * @function name    : forgotPassword()
    * @description      : send password reset link or code on mail    
    * @access           : public
    * @param            : $data array
    * @return           : void  
    */
    public function forgotPassword($data=array()) {

        log_message('error', 'in forgotPassword function');
        if(isset($data['email']) && $data)
        {
            log_message('error', 'data and email is set');
            $this->db->from('tbl_users');
            $this->db->where('email',$data['email']);
            $this->db->where('active',1);
            $query=$this->db->get();

            if($query->num_rows()>0) {
                log_message('error', 'query has result');
                $res = $query->row_array();
                $RandNo = $this->commons->generate_randomnumber(4);
                $acc_code = $RandNo.$res['id'];
                    
                $this->db->set('forgotten_password_code', $acc_code);
                $this->db->where('id',$res['id']);
                $this->db->update('tbl_users');
                log_message('error', 'set forgot password code');
               
               //=== Send Email ====
                    $Template = $this->mailer->Tpl_Email('forgot_password_api',$this->commons->encode($data['email']));
                    $Recipient = $res['email'];
                    $Filter = array(
                        '{{NAME}}'          =>$res['first_name'].' '.$res['last_name'],
                        '{{RESET_CODE}}'    =>$acc_code,
                        // '{{RESET_LINK}}'    =>base_url('api/users/doforgotpassword/'.$res['id'].'/'.$this->commons->encode($acc_code))
                    );
                    $this->mailer->Send_Singal_Html_Email($Recipient,$Template,$Filter);
                    log_message('error', 'send mail and return true');
                //===================
                return TRUE; 
            } 
            else 
            {
                log_message('error', 'return false');
                return FALSE;
            }
        }
        else 
        {
            log_message('error', 'return false');
            return FALSE;
        }       
    }

    /**
    * 
    * @function name : VerifyForgotPasswordCode()
    * @description   : verify user forgot password code
    * @access        : public
    * @return        : void
    *
    */
    public function VerifyForgotPasswordCode($data = array())
    {
        log_message('error', 'in VerifyForgotPasswordCode function');
        $this->db->select('id,username,userimage,email,active');
        $this->db->from('tbl_users');
        $this->db->where('email',$data['email']);
        $this->db->where('forgotten_password_code',$data['code']);
        $result = $this->db->get();
        return $result;
        log_message('error', 'return result');
    }

    /**
    * 
    * @function name : UpdatePassword()
    * @description   : Reset or update user account password
    * @access        : public
    * @return        : void
    *
    */
    public function UpdatePassword($data = array())
    {  
        log_message('error', 'in UpdatePassword function');
        $this->db->set('password', password_hash($data['new_password'], PASSWORD_DEFAULT));
        $this->db->where('email',$data['email']);
        $this->db->where('forgotten_password_code', $data['code']);
        $this->db->update('tbl_users');
        if($this->db->affected_rows())
        {
            log_message('error', 'UpdatePassword return true');
            return TRUE;
        }
        else
        {
            log_message('error', 'UpdatePassword return false');
            return FALSE;
        }
    }

    /**
    * 
    * @function name : UpdateForgotCode()
    * @description   : update "forgotten_password_code" field after user can successfully reset his password
    * @access        : public
    * @return        : void
    *
    */
    public function UpdateForgotCode($email)
    {
        log_message('error', 'in UpdateForgotCode function');
        $this->db->set('forgotten_password_code',NULL);
        $this->db->where('email',$email);
        $this->db->update('tbl_users');
        log_message('error', 'UpdateForgotCode return true');
        return true;
    }

    /**
    * 
    * @function name : checkPassword()
    * @description   : check old password
    * @access        : public
    * @return        : array
    *
    */
    public function checkPassword($id) 
    {
        log_message('error', 'in checkPassword function');
        $this->db->from('tbl_users');
        $this->db->where('id',$id);  
        $result = $this->db->get();
        return $result;
        log_message('error', 'checkPassword function return result');
    }

    /**
    * 
    * @function name : update_forgot_password_field()
    * @description   : update forgot password field
    * @param         : email, reset_link
    * @return        : void
    *
    */
    public function update_forgot_password_field($email,$reset_link,$password_updated = '')
    {
        log_message('error', 'in update_forgot_password_field function');
        $data = array(
        'forgotten_password_code' => $reset_link,
        'forgotten_password_time' => ($password_updated) ? '' : date('YmdHis'),
        ) ;

        $this->db->set($data);
        $this->db->where('email', $email)
                 ->update('tbl_users') ;

        if($this->db->affected_rows() > 0)
        {
            log_message('error', 'update_forgot_password_field function return true');
            return true ;
        }
        else
        {
            log_message('error', 'update_forgot_password_field function return false');
            return false ;
        }
    }

    /**
    * 
    * @function name    : getEmailTemplateByCode()
    * @description      : get email_template record by template_code
    * @param            : $template_code The template_code  that you want
    * @return           : array The selected template_code array
    *
    */
    public function getEmailTemplateByCode($template_code)
    {
        log_message('error', 'in getEmailTemplateByCode function');
        $this->db->from('email_template');
        $this->db->where('template_code',$template_code);
        $query=$this->db->get();
        return $query->row_array();
        log_message('error', 'getEmailTemplateByCode function return row array');
    }

    /**
    * 
    * @function name : check_email()
    * @description   : check registered email
    * @param         : email
    * @return        : user data
    *
    */
    public function check_email($email)
    {
        log_message('error', 'in check_email function');
        $query = $this->db->get_where('tbl_users',array('email' => $email));
        return $query ;
        log_message('error', 'check_email function return query in result');
    }

    /**
    * 
    * @function name : check_reset_link()
    * @description   : check forgot password link
    * @param         : link
    * @return        : boolean
    *
    */
    public function check_reset_link($link)
    {
        log_message('error', 'in check_reset_link function');
        $data = $this->db->get_where('tbl_users',array('forgotten_password_code'=>$link))->row_array();
        return $data ;
        log_message('error', 'check_reset_link function return data');
    }
}