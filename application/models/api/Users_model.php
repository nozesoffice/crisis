<?php
/**
* 
* @file name   : Users_model
* @Auther      : Bhavin Narola
* @Date        : 08-03-2017
* @Description : Collection of various common function related to user database operation.
*
*/
class Users_model extends CI_Model 
{
    /**
    * 
    * @function name 	: __construct()
    * @description   	: initialize variables
    * @param   		    : void
    * @return        	: void
    *
    */
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('mailer');
       
    }

     /**
    * 
    * @function name : getAllUser()
    * @description   : get all users record
    * @access        : public
    * @return        : array The selected users array
    *
    */
    public function getAllUser() 
    {
        $this->db->select('id,username,userimage,email,created_on,active,first_name,last_name,company,phone');
        $this->db->where('id !=', 1);
        $this->db->from('tbl_users');
        $query=$this->db->get();
        return $query->result_array();
    }

    /**
    * 
    * @function name : getUserById()
    * @description   : get user record by user_id
    * @access        : public
    * @param   	     : int $user_id The user id that you want
    * @return        : array The selected users array
    *
    */
    public function getUserById($user_id) 
    {
    	$this->db->select('id,username,userimage,email,created_on,active,first_name,last_name,company,phone');
        $this->db->from('tbl_users');
        $this->db->where('id',(int)$user_id);
        $this->db->where('active',1);
        $query=$this->db->get();
        return $query->row_array();
    }

    /**
    * 
    * @function name : addUserData()
    * @description   : add Register User record in database
    * @access        : public
    * @return        : int last inserted User record id
    *
    */
    public function addUserData($data=array()) 
    {
        if($data)
        {
            $activation_code = $this->commons->generate_randomnumber(6);
            
            $this->db->set('first_name', $data['firstname']);
            $this->db->set('last_name', $data['lastname']);
            $this->db->set('username',$data['username']);
            $this->db->set('email', $data['email']);
            $this->db->set('phone', $data['phone']);
            $this->db->set('userimage',$data['userimage']);
            $this->db->set('tbl_roles_role_id',$data['role_id']);
            $this->db->set('ip_address', $_SERVER['REMOTE_ADDR']);
            $this->db->set('activation_code',$activation_code);
            $this->db->set('password', password_hash($data['password'], PASSWORD_DEFAULT));
            $this->db->set('active', 0);
            $this->db->set('created_on',date('Y-m-d h:i:sa'));
            $this->db->insert('tbl_users');
            $user_id = $this->db->insert_id();
            
            if($user_id > 0) 
            {
                $name = $data['firstname']." ".$data['lastname'];
                $email = $data['email'];
                $activation_link = base_url('account/register/active_user/').$this->commons->encode($user_id).'/'.$this->commons->encode($activation_code);
                
                //=== Send Email ====//
                $Template = $this->mailer->Tpl_Email('user_signup',$this->commons->encode($email));
                    $Recipient = $email;
                    $Filter = array(
                        '{{NAME}}' =>$name,
                        '{{ACTIVATION_LINK}}' => $activation_link,
                    );
                $this->mailer->Send_Singal_Html_Email($Recipient,$Template,$Filter);
                //===================
            } 
            return $user_id;    
        }
        else
        {
            return FALSE;
        }
    }

    /**
    * 
    * @function name : updateUserData()
    * @description   : edit user record in database
    * @access        : public
    * @return        : void
    *
    */
    public function updateUserData($data=array()) 
    {
        if($data)
        {
            $image = $data['userimage'];

            if(isset($data['user_id']) && $data['user_id'] !=="")
            {
                $this->db->set('first_name', $data['firstname']);
                $this->db->set('last_name', $data['lastname']);
                $this->db->set('username', $data['username']);
                $this->db->set('email', $data['email']);
                $this->db->set('phone', $data['phone']);
                $this->db->set('ip_address', $_SERVER['REMOTE_ADDR']);
                $this->db->set('modified_on',date('Y-m-d h:i:sa'));
                if(!empty($image)){
                    $this->db->set('userimage',$image);
                }
                $this->db->where('id',(int)$data['user_id']);
                return $this->db->update('tbl_users');       
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }   
    }

    /**
    * 
    * @function name : deleteUserData()
    * @description   : delete user record in database
    * @access        : public
    * @return        : void
    *
    */
    public function deleteUserData($id) 
    {
        $this->db->where('id',$id);
        $result = $this->db->delete('tbl_users');
        if($this->db->affected_rows()>0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
    * 
    * @function name : Register()
    * @description   : New user registration
    * @access        : public
    * @return        : void
    *
    */
    public function Register($data=array()) 
    {
        if($data)
        {
            $activation_code = $this->commons->generate_randomnumber(6);
            
            $this->db->set('first_name', $data['firstname']);
            $this->db->set('last_name', $data['lastname']);
            $this->db->set('username',$data['username']);
            $this->db->set('email', $data['email']);
            $this->db->set('phone', $data['phone']);
            $this->db->set('tbl_roles_role_id',$data['role_id']);
            $this->db->set('ip_address', $_SERVER['REMOTE_ADDR']);
            $this->db->set('activation_code',$activation_code);
            $this->db->set('password', password_hash($data['password'], PASSWORD_DEFAULT));
            $this->db->set('active', 0);
            $this->db->set('created_on',date('Y-m-d h:i:sa'));
            $this->db->insert('tbl_users');
            $user_id = $this->db->insert_id();
            
            if($user_id > 0) 
            {
                $name = $data['firstname']." ".$data['lastname'];
                $email = $data['email'];
                $activation_link = base_url('account/register/active_user/').$this->commons->encode($user_id).'/'.$this->commons->encode($activation_code);
                
                //=== Send Email ====//
                $Template = $this->mailer->Tpl_Email('user_signup',$this->commons->encode($email));
                    $Recipient = $email;
                    $Filter = array(
                        '{{NAME}}' =>$name,
                        '{{ACTIVATION_LINK}}' => $activation_link,
                    );
                $this->mailer->Send_Singal_Html_Email($Recipient,$Template,$Filter);
                //===================
            } 
            return $user_id;    
        }
        else
        {
            return FALSE;
        }
    }


    /**
    * 
    * @function name    : Login()
    * @description      : Check UserName and Password for Aithantication
    * @access           : public
    * @param            : void
    * @return           : void
    *
    */
    public function Login($data=array())    
    { 
        if($data)
        {
            $this->db->select('id,first_name,last_name,phone,email,active,username,password,company,userimage,tbl_roles_role_id');
            $this->db->from('tbl_users');
            $this->db->where('email',$data['email']);
            $query=$this->db->get();
                   
            if ($query->num_rows() == 0)  return FALSE; // invalid id or password
                $result = $query->row_array();
            
            if ($result['active'] == 0)  return $result;; // Inactive Account
            $token = array();
            $token['id'] = $result['id'];
            $this->db->set('last_login',date('Y-m-d h:i:sa'))->where('id',$result['id'])->update('tbl_users');    
            return $result;
        }         
    }

    /** 
    * @function name    : forgotPassword()
    * @description      : send password reset link or code on mail    
    * @access           : public
    * @param            : $data array
    * @return           : void  
    */
    public function forgotPassword($data=array()) {

        if(isset($data['email']) && $data)
        {
            $this->db->from('tbl_users');
            $this->db->where('email',$data['email']);
            $this->db->where('active',1);
            $query=$this->db->get();

            if($query->num_rows()>0) {
                $res = $query->row_array();
                $RandNo = $this->commons->generate_randomnumber(4);
                $acc_code = $RandNo.$res['id'];
                    
                $this->db->set('forgotten_password_code', $acc_code);
                $this->db->where('id',$res['id']);
                $this->db->update('tbl_users');
               
               //=== Send Email ====
                    $Template = $this->mailer->Tpl_Email('forgot_password',$this->commons->encode($data['email']));
                        $Recipient = $res['email'];
                        $Filter = array(
                            '{{NAME}}' =>$res['first_name'].' '.$res['last_name'],
                            '{{RESET_CODE}}' =>$acc_code,
                            '{{RESET_LINK}}' =>base_url('api/users/doforgotpassword/'.$res['id'].'/'.$this->commons->encode($acc_code))
                        );
                    $this->mailer->Send_Singal_Html_Email($Recipient,$Template,$Filter);
                //===================
                return TRUE; 
            } 
            else 
            {
                return FALSE;
            }
        }
        else 
        {
            return FALSE;
        }       
    }

    /**
    * 
    * @function name : VerifyForgotPasswordCode()
    * @description   : verify user forgot password code
    * @access        : public
    * @return        : void
    *
    */
    public function VerifyForgotPasswordCode($data = array())
    {
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('email',$data['email']);
        $this->db->where('forgotten_password_code',$data['code']);
        $result = $this->db->get();
        return $result;
    }

    /**
    * 
    * @function name : UpdatePassword()
    * @description   : Reset or update user account password
    * @access        : public
    * @return        : void
    *
    */
    public function UpdatePassword($data = array())
    {  
        $this->db->set('password', password_hash($data['new_password'], PASSWORD_DEFAULT));
        $this->db->where('email',$data['email']);
        $this->db->where('forgotten_password_code', $data['code']);
        $this->db->update('tbl_users');
        if($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
    * 
    * @function name : UpdateForgotCode()
    * @description   : update "forgotten_password_code" field after user can successfully reset his password
    * @access        : public
    * @return        : void
    *
    */
    public function UpdateForgotCode($email)
    {
        $this->db->set('forgotten_password_code',NULL);
        $this->db->where('email',$email);
        $this->db->update('tbl_users');
        return true;
    }

    /**
    * 
    * @function name : editPassword()
    * @description   : edit customer password record in database
    * @access        : public
    * @return        : void
    *
    */
    public function editPassword($data=array()) 
    {
        $this->db->set('password', password_hash($data['password'], PASSWORD_DEFAULT));
        $this->db->set('ip_address', $_SERVER['REMOTE_ADDR']);
        $this->db->where('id',(int)$data['user_id']);
        $this->db->update('tbl_users');   
        if($this->db->affected_rows())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
    * 
    * @function name : checkPassword()
    * @description   : check old password
    * @access        : public
    * @return        : array
    *
    */
    public function checkPassword($id) 
    {
        $this->db->from('tbl_users');
        $this->db->where('id',$id);  
        $result = $this->db->get();
        return $result;
    }

           

}