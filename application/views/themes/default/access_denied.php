<div class="content-wrapper">
<section class="content">
      <div class="row">
      
    	<div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="panel-content">
            	<h1 style="font-size:75px;" class="text-muted text-center">
                <i class="icon-lock"></i></h1>
                <h1 class="text-danger text-center"><strong>Access Denied</strong></h1>
            	<h2 class=" text-center">You dont have a permission access the url:</h2>
                <p style="padding:5px;" class="text-primary text-center"><?=base_url($url)?></p>
                <h4 class="text-muted text-center">
                	In order to access this module, you need to contact system administrator. 
                </h4>
            </div>
        </div>
    </div>
</div>
