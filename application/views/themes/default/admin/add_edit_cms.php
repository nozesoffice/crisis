<!-- Ckeditor -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/ckeditor/ckeditor.js"></script>

<div class="content-wrapper">
  <section class="content-header">
        <h1><?php echo $heading; ?></h1>
        <ol class="breadcrumb">

          <?php foreach ($breadcrumb as $bkey => $bvalue) {
          ?>
            <li class="<?php echo $bvalue['class']; ?>">
              <?php if($bvalue['class']) { 
              echo $bvalue['text'];
               } else { ?>
              <a href="<?php echo $bvalue['href']; ?>"><?php echo $bvalue['text']; ?></a>
              <?php } ?>
            </li>
          <?php
          } ?>
          <!-- <li><a href="<?php echo base_url('dashboard/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="<?php echo base_url('contact') ?>">Contact</a></li>
          <li class="active">Document Types</li> -->
        </ol>
  </section>
   
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Success message -->
      <?php if(isset($success) && $success !="" ){ ?>
            <div class="alert alert-success alert-dismissible" id="success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $success; ?>
            </div>
      <?php } ?>
      <!-- Success message -->

      <!-- error message -->
      <?php if(isset($error) && $error !="" ){ ?>
          <div class="alert alert-danger alert-dismissible" id="error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error; ?>
          </div>
      <?php } ?>
      <!-- error message -->         
      <!-- Left col -->
      <div class="col-md-12">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">
             
            <?php 
            if(isset($edit_data))
            {
              echo "Edit Page data ";
            }
            else

            {
              echo "Add New Page" ;
            }
           
             ?>
            </h3>
            <div class="box-tools pull-right">
            </div>
          </div>
         
          <!-- /.box-header -->
          <div class="box-body">
            <form class="" method="post" id="user_data" action="<?php echo base_url($action) ; ?>">
              <div class="box-body">
                <input type="hidden" name="hidden_cms_id" id="hidden_cms_id" value="<?php echo isset($edit_data) ? $edit_data['Id'] : '' ; ?>">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="seo_title"><span class="req">*</span>SEO Title:</label>
                      <input placeholder="SEO Title" type="text" value="<?php echo isset($edit_data) ? $edit_data['SeoTitle'] : set_value('seo_title') ; ?>" name="seo_title" id="seo_title" class="form-control">
                      <?php echo form_error('seo_title'); ?>
                  </div>

                  <div class="form-group">
                      <label for="title"><span class="req">*</span>Title:</label>
                      <input placeholder="Title" type="text" value="<?php echo isset($edit_data) ? $edit_data['Title'] : set_value('title') ; ?>" name="title" id="title" class="form-control">
                      <?php echo form_error('title'); ?>
                  </div>

                  <div class="form-group SEOWrap">
                      <label for="keywords">SEO Keywords:</label>
                      <select name="keywords[]" id="keywords" class="form-control select" multiple="multiple"></select>
                      <?php echo form_error('keywords'); ?>
                  </div>


                  <div class="form-group">
                      <label for="description"><span class="req">*</span>Description:</label>
                      <textarea class="form-control" placeholder="Description" id="editor1" name="description"><?php echo isset($edit_data) ? $edit_data['Description'] : set_value('description') ; ?></textarea>                      
                      <?php echo form_error('description'); ?>
                  </div>                 
                
                  <div class="form-group">   
                      <input type="submit" name="submit" class="btn btn-primary submit" value="Save"/>               
                     <button type="reset" class="btn btn-danger">Reset</button>
                  </div> 

                </div>
                
              </div>
              
            </form>
          </div>
          <div class="clearfix"></div>   
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<script type="text/javascript">
   $(".select").select2({
    tags: true,
    tokenSeparators: [',', ' ']
    }); 

  CKEDITOR.replace( 'editor1',
    {
        filebrowserBrowseUrl :'<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/browser/default/browser.html?Connector=<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl : '<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/browser/default/browser.html?Type=Image&Connector=<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl :'<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/browser/default/browser.html?Type=Flash&Connector=<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl  :'<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl : '<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl : '<?=base_url('assets/admin');?>/ckeditor/plugins/filemanager/connectors/php/upload.php?Type=Flash',
        allowedContent:true
    });
</script>
<style type="text/css">
.req {
  color: red;
}
.alert{
    width: 98%;
    margin-left: 1%;
}
.error {
color:red;
font-size:13px;
margin-bottom:-15px;
}
.error-message{
  color:red;

}
</style>
  <?php 
  if(!empty($edit_data))
  {
    $keywords = explode(",",$edit_data['SeoKeyword']);
  }
  ?>
<script type="text/javascript">
    <?php if(!empty($keywords)) : ?>
    <?php foreach ($keywords as $key => $p) { ?>
        $('#keywords').select2({
        tags: true,
        tokenSeparators: [',', ' ']
        }).append('<option selected="selected" value="<?php echo $p;?>"><?php echo $p ;?></option>');
        
    <?php } ?> 
    <?php endif;?>
</script>