<div class="content-wrapper">
  <section class="content-header">
        <h1><?php echo $heading; ?></h1>
        <ol class="breadcrumb">

          <?php foreach ($breadcrumb as $bkey => $bvalue) {
          ?>
            <li class="<?php echo $bvalue['class']; ?>">
              <?php if($bvalue['class']) { 
              echo $bvalue['text'];
               } else { ?>
              <a href="<?php echo $bvalue['href']; ?>"><?php echo $bvalue['text']; ?></a>
              <?php } ?>
            </li>
          <?php
          } ?>
          <!-- <li><a href="<?php echo base_url('dashboard/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="<?php echo base_url('contact') ?>">Contact</a></li>
          <li class="active">Document Types</li> -->
        </ol>
  </section>
   
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Success message -->
      <?php if(isset($success) && $success !="" ){ ?>
            <div class="alert alert-success alert-dismissible" id="success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $success; ?>
            </div>
      <?php } ?>
      <!-- Success message -->

      <!-- error message -->
      <?php if(isset($error) && $error !="" ){ ?>
          <div class="alert alert-danger alert-dismissible" id="error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error; ?>
          </div>
      <?php } ?>
      <!-- error message -->         
      <!-- Left col -->
      <div class="col-md-12">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">
             
            <?php 
            if(isset($edit_data))
            {
              echo "Edit Role data ";
            }
            else

            {
              echo "Add New Role" ;
            }
           
             ?>
            </h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form class="" method="post" id="user_data" action="<?php echo base_url($action) ; ?>">
              <div class="box-body">
                <input type="hidden" name="hidden_role_id" id="hidden_role_id" value="<?php echo isset($edit_data) ? $edit_data['role_id'] : '' ; ?>">
                <div class="col-md-12">

                  <div class="form-group">
                      <label for="first_name"><span class="req">*</span>Role Name:</label>
                      <input placeholder="Role name" type="text" value="<?php echo isset($edit_data) ? $edit_data['role_name'] : set_value('role_name') ; ?>" name="role_name" id="role_name" class="form-control">
                      <?php echo form_error('role_name'); ?>
                  </div>


                  <div class="form-group">
                      <label for="role_description">Role Description:</label>
                      <textarea class="form-control" placeholder="Description" name="role_description"><?php echo isset($edit_data) ? $edit_data['role_description'] : set_value('role_description') ; ?></textarea>                      
                      <?php echo form_error('role_description'); ?>
                  </div>                 

                 
                  <div class="form-group" >        
                    <label for="role_status"><span class="req">*</span>Role Status:</label> 
                      <select class="form-control" name="role_status" id="role_status">
                        <option value="">Select Status</option>
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                      </select>
                      <?php echo form_error('role_status'); ?>
                  </div>

                  
                  <script type="text/javascript">
                    $("#role_status").val('<?php echo isset($edit_data['role_status']) ?  $edit_data['role_status'] : set_value('role_status') ; ?>');
                  </script>
                  

                 

                 
                  <div class="form-group">   
                      <input type="submit" name="submit" class="btn btn-info submit" value="Save"/>               
                     <button type="reset" class="btn btn-danger">Reset</button>
                  </div> 

                </div>
                
              </div>
              
            </form>
          </div>
          <div class="clearfix"></div>   
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<style type="text/css">
.req {
  color: red;
}
.alert{
    width: 98%;
    margin-left: 1%;
}
.error {
color:red;
font-size:13px;
margin-bottom:-15px;
}
.error-message{
  color:red;

}
</style>
