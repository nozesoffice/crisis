<div class="content-wrapper">
  <section class="content-header">
        <h1>User</h1>
        <ol class="breadcrumb">

          <?php foreach ($breadcrumb as $bkey => $bvalue) {
          ?>
            <li class="<?php echo $bvalue['class']; ?>">
              <?php if($bvalue['class']) { 
              echo $bvalue['text'];
               } else { ?>
              <a href="<?php echo $bvalue['href']; ?>"><?php echo $bvalue['text']; ?></a>
              <?php } ?>
            </li>
          <?php
          } ?>
          
        </ol>
  </section>
   
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Success message -->
      <?php if(isset($success) && $success !="" ){ ?>
            <div class="alert alert-success alert-dismissible" id="success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $success; ?>
            </div>
      <?php } ?>
      <!-- Success message -->

      <!-- error message -->
      <?php if(isset($error) && $error !="" ){ ?>
          <div class="alert alert-danger alert-dismissible" id="error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error; ?>
          </div>
      <?php } ?>
      <!-- error message -->         
      <!-- Left col -->
      <div class="col-md-12">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">
             
            <?php 
            if(isset($edit_data))
            {
              echo "Edit User data ";
            }
            else

            {
              echo "Add New User" ;
            }
           
             ?>
            </h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form class="" method="post" id="user_data" action="<?php echo base_url($action) ; ?>">
              <div class="box-body">
                <input type="hidden" name="hidden_user_id" id="hidden_user_id" value="<?php echo isset($edit_data) ? $edit_data['user_id'] : '' ; ?>">
                <div class="col-md-12">
                  <div class="form-group">
                      <label for="first_name">First Name:</label>
                      <input placeholder="User first name" type="text" value="<?php echo isset($edit_data) ? $edit_data['first_name'] : set_value('first_name') ; ?>" name="first_name" id="first_name" class="form-control">
                      <?php echo form_error('first_name'); ?>
                  </div>
                  <div class="form-group">
                    <label for="last_name">Last Name :</label>
                    <input placeholder="User last name" type="text" value="<?php echo isset($edit_data) ? $edit_data['last_name'] : set_value('last_name') ; ?>" class="form-control" id="last_name" name="last_name">
                    <?php echo form_error('last_name'); ?>
                  </div>

                  <div class="form-group">
                    <label for="user_name"><span class="req">*</span>User Name :</label>
                    <input placeholder="User name" type="text" value="<?php echo isset($edit_data) ? $edit_data['username'] : set_value('user_name') ; ?>" class="form-control" id="user_name" name="user_name">
                    <?php echo form_error('user_name'); ?>
                  </div>
                  
                
                  <div class="form-group">
                    <label for="email"><span class="req">*</span>Email :</label>
                    <input type="email" value="<?php echo isset($edit_data) ? $edit_data['email'] : set_value('email') ; ?>" class="form-control" id="email" name="email" placeholder="User Email">
                    <?php echo form_error('email'); ?>
                  </div>

                  <div class="form-group">
                    <label for="phone"><span class="req">*</span>Contact No :</label>
                    <input placeholder="Contact Number" type="text" value="<?php echo isset($edit_data) ? $edit_data['phone'] : set_value('phone') ; ?>" class="form-control" id="phone" name="phone">
                    <?php echo form_error('phone'); ?>
                  </div>
                
                  <div class="form-group">
                    <input type="hidden" name="user_id" id="user_id">        
                    <label for="user_role"><span class="req">*</span>User Role:</label>
                      <select class="form-control" name="user_role" id="user_role">
                        <option value="">Select Role</option>
                        <?php foreach ($roles as $key => $value) { ?>
                        <option value="<?php echo $value['role_id'] ?>"><?php echo $value['role_name'] ?></option>
                        <?php } ?>
                        
                      </select>
                      <?php echo form_error('user_role'); ?>
                  </div>

                  <?php if(isset($edit_data)) { ?>
                  <script type="text/javascript">
                    $("#user_role").val(<?php echo $edit_data['tbl_roles_role_id'] ;?>);
                  </script>
                  <?php } else { ?>
                  <script type="text/javascript">
                    $("#user_role").val(<?php echo set_value('user_role') ;?>);
                  </script>
                  <?php } ?>

                  <?php if(isset($edit_data)) { ?>
                  <div id="user_login_status" class="form-group" >        
                    <label for="user_status"><span class="req">*</span>User Status:</label> 
                      <select class="form-control" name="user_status" id="user_status">
                        <option value="">Select Status</option>
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                      </select>
                      <?php echo form_error('user_status'); ?>
                  </div>

                  <script type="text/javascript">
                    $("#user_status").val(<?php echo $edit_data['active'] ;?>);
                  </script>

                  <?php } else { ?>
                  <script type="text/javascript">
                    $("#user_status").val(<?php echo set_value('user_status') ;?>);
                  </script>
                  <?php } ?>

                  <div class="form-group" id="user_password">
                    <label for="password"><span class="req">*</span>Password :</label>
                      <input type="password"  class="form-control" id="password" placeholder="Enter Password" name="password" value="<?php echo set_value('password') ; ?>">
                      <?php echo form_error('password'); ?>
                  </div>

                  <div class="form-group" id="user_cpassword">
                    <label for="cpassword"><span class="req">*</span>Confirm Password :</label>
                      <input type="password" class="form-control" id="cpassword" placeholder="Enter Confirm Password" value="<?php echo set_value('cpassword') ; ?>" name="cpassword">
                      <?php echo form_error('cpassword'); ?>
                  </div>

                  <div class="form-group">   
                      <input type="submit" name="submit" class="btn btn-info submit" value="Save"/>               
                     <button type="reset" class="btn btn-danger">Reset</button>
                  </div> 

                </div>
                
              </div>
              
            </form>
          </div>
          <div class="clearfix"></div>   
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<style type="text/css">
.req {
  color: red;
}
.alert{
    width: 98%;
    margin-left: 1%;
}
.error {
color:red;
font-size:13px;
margin-bottom:-15px;
}
.error-message{
  color:red;

}
</style>
