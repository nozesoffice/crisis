<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password
      </h1>
      <ol class="breadcrumb">
        <?php foreach ($breadcrumb as $bkey => $bvalue) {
          ?>
            <li class="<?php echo $bvalue['class']; ?>">
              <?php if($bvalue['class']) { 
              echo $bvalue['text'];
               } else { ?>
              <a href="<?php echo $bvalue['href']; ?>"><?php echo $bvalue['text']; ?></a>
              <?php } ?>
            </li>
          <?php
          } ?>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php 
        if($this->session->flashdata('success')){
       ?>
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>Success !</strong> <?php echo $this->session->flashdata('success') ; ?>
      </div>
      <?php } ?>
      <?php 
        if($this->session->flashdata('error')){
       ?>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>Error !</strong> <?php echo $this->session->flashdata('error') ; ?>
      </div>
      <?php } ?>
      <div class="row">
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <div class="tab-content">
              <div class=" active tab-pane" id="settings">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo base_url('change_password'); ?>">    
                 
                  <h5>Password Change</h5>
                  <hr>
                    <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"><span class="req">*</span> Old Password</label>
                    <div class="col-sm-10">
                      <input type="Password" name="old_password" value="<?php echo set_value('old_password'); ?>" class="form-control" placeholder="Old Password">
                      <?php echo form_error('old_password'); ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"><span class="req">*</span> New Password</label>
                    <div class="col-sm-10">
                      <input type="Password" name="new_password" value="<?php echo set_value('new_password'); ?>" class="form-control" placeholder="New Password">
                      <?php echo form_error('new_password'); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"><span class="req">*</span> Confirm New Password</label>
                    <div class="col-sm-10">
                      <input type="Password" name="confirm_new_password" value="<?php echo set_value('confirm_new_password'); ?>" class="form-control" placeholder="Confirm New Password">
                      <?php echo form_error('confirm_new_password'); ?>
                    </div>
                  </div>
                   <br>
              
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" name="submit" value="1" class="btn btn-info">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<style type="text/css">
  .req {
    color: red;
  }
  .alert{
      /*width: 98%;
      margin-left: 1%;*/
  }
  .error {
  color:red;
  font-size:13px;
  margin-bottom:-15px;
  }
  .error-message{
    color:red;
  }
</style>
