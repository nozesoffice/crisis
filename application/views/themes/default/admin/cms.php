<div class="content-wrapper">

<section class="content-header">
      <h1>All <?php echo $heading; ?></h1>

     <ol class="breadcrumb">

      <?php foreach ($breadcrumb as $bkey => $bvalue) {
          ?>
            <li class="<?php echo $bvalue['class']; ?>">
              <?php if($bvalue['class']) { 
              echo $bvalue['text'];
               } else { ?>
              <a href="<?php echo $bvalue['href']; ?>"><?php echo $bvalue['text']; ?></a>
              <?php } ?>
            </li>
          <?php
          } ?>
       
      </ol>
      
    </section>
<section class="content">
      <div class="row">
        <div class="col-md-12">

        <!-- Success message -->
    
        <?php if ($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" id="success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php endif; ?>
     

      <!-- Success message -->

      <!-- error message -->
     
        <?php if ($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible" id="error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
     

       <!-- error message -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $heading; ?></h3> 
              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <a href="<?php echo base_url('cms/add');?>" class="btn btn-info AddItem">Add New Page</a>

                             <div class="dropdown Action_Dropdown">
                             <a class="dropdown-toggle Action_custom btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false">
                                Action
                                <span class="fa fa-caret-down"></span>
                            </a>
                              <ul class="dropdown-menu Action_menu">
                                <li> <a href="#" class="action-delete" 
                                        onClick="return action_delete(document.recordlist)" >
                                        Delete
                                        </a></li>
                                <li><a href="#" class="action-delete" 
                                        onClick="return action_active(document.recordlist)" >
                                        Publish
                                        </a></li>
                                <li><a href="#" class="action-delete" o
                                        onClick="return action_deactive(document.recordlist)">
                                        Unpublish
                                        </a></li>
                              </ul>
                            </div>


            <div class="row" style="margin:0;">
                <div class="col-sm-12">
                    <div class="box-body">
                      <div class="table-responsive">
                        <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('admin/cms/action') ?>">
                        <table class="table no-margin" id="example1">
                          <thead>
                          <tr>

                            <th width="5%"><input class="text-center" type="checkbox" name="allchk" onclick="$('.chk').prop('checked', $(this).prop('checked'));"></th>                         
                            <th width="20%">Title</th>                           
                            <th width="20%">SEO Title</th>                           
                            <th width="25%">SEO Keyword</th>                            
                            <th width="15%">Status</th>
                            <th width="15%">Action</th>
                          </tr>
                          </thead>
                          <tbody>                          
                          <?php 

                          if(isset($cms_data)){                            
                            foreach ($cms_data as $cms) {                              
                           ?>                    
                          <tr>
                            
                            <td width="5%" valign="middle"><input type="checkbox" class="chk" name="Id_List[]" id="Id_List[]" value="<?php echo $cms['Id']?>"></td>
                            <td width="20%"><?php echo $cms['Title']; ?></td>
                            <td width="20%"><?php echo $cms['SeoTitle']; ?></td>
                                             
                            <td width="25%"><?php echo $cms['SeoKeyword']; ?></td>                     
                            <td width="15%"><?php echo ($cms['Status'] == 1) ? "Active" : "Deactivate"; ?></td>
                            <td width="15%">
                              <a href="<?php echo base_url('cms/edit/'.$cms['Id']) ;?>" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></a>
                              
                          <!--     <a href="<?php echo base_url('cms/access/'.$cms['Id']) ;?>" class="btn btn-danger" title="Assign Rights"><i class="fa fa-server"></i></a> -->

                              <a onclick="delete_cms('<?php echo $cms['Id'] ?>')" class="btn btn-danger" title="Delete"><i class="fa fa-trash-o"></i></a>

                            </td>
                          </tr>
                          <?php                           
                            } }
                           ?>                                                                  
                          </tbody>
                        </table>
                        <input type="hidden" name="action" />  
                        </form>
                      </div>
                    </div>
                </div>
            </div>
           </div> 
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>

   </div>


<style type="text/css">
.req {
  color: red;
}

.error {
color:red;
font-size:13px;
margin-bottom:-15px;
}
.error-message{
  color:red;

}
</style>
<script>
  $(function () {
    $("#example1").DataTable({
      language: {
        paginate: {
          next: 'Next', // or '→'
          previous: 'Previous' // or '←' 
        }     
      },
      "columnDefs": [
          { "orderable": false, "targets": 5, "targets": 0 },
          { "searchable": false, "targets": 5 },
        ],
      "pageLength": 10,
    });
  });
</script>
<script type="text/javascript">
function delete_cms(id)
{
  var c = window.confirm('Are you sure want to delete?');

  if(c)
  {
    window.location.href = '<?php echo base_url('cms/delete') ?>/'+id;
  }
  
}
</script>

<script>
 jQuery(document).ready(function(){
   jQuery('.Action_custom').click(function(){
   jQuery('.Action_menu').toggle('slow');
   });
 })
</script>

<script type="text/javascript">
  //==========================================================================

function CheckUncheckAll()
{
    var checks = document.getElementsByName('Id_List[]');
    for (i = 0; i < checks.length; i++)
    {
        if(checks[i].checked == true) 
            checks[i].checked = false;
        else
            checks[i].checked = true;
    }
}
//====================================================================
function action_active(frm)
{
    with(frm)
    {
        var flag = false;
        str = '';
        field = document.getElementsByName('Id_List[]');
        for (i = 0; i < field.length; i++)
        {
            if(field[i].checked == true)
            { 
                flag = true;
                break;
            }
            else
                field[i].checked = false;
        }
        if(flag == false)
        {
            alert("Please select atleast one record");
            return false;
        }
    }
    frm.action.value = "action_active";
    frm.submit();
    return true ;
}
//================================================================
function action_deactive(frm)
{
    with(frm)
    {
        var flag = false;
        str = '';
        field = document.getElementsByName('Id_List[]');
        for (i = 0; i < field.length; i++)
        {
            if(field[i].checked == true)
            { 
                flag = true;
                break;
            }
            else
                field[i].checked = false;
        }
        if(flag == false)
        {
            alert("Please select atleast one record");
            return false;
        }
    }
    frm.action.value = "action_deactive";
    frm.submit();
    return true ;
}
//===================================================================
function action_delete(frm)
{
    with(frm)
    {
        var flag = false;
        str = '';
        field = document.getElementsByName('Id_List[]');
        for (i = 0; i < field.length; i++)
        {
            if(field[i].checked == true)
            { 
                flag = true;
                break;
            }
            else
                field[i].checked = false;
        }
        if(flag == false)
        {
            alert("Please select atleast one record");
            return false;
        }
    }
    if(confirm("Are you sure to delete selected records ?"))
    {
        frm.action.value = "action_delete";
        frm.submit();
        return true ;
    }
}
</script>
