<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

    <!-- Success Message -->

    <?php 
      if($success = $this->session->flashdata('success'))
      {
    ?>

    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>Fine !</strong> <?php echo $success ; ?>
    </div>

    <?php
      }
     ?>

     <!-- Error Message -->

     <?php 
      if($error = $this->session->flashdata('error'))
      {
    ?>

    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>Oops !</strong> <?php echo $error ; ?>
    </div>

    <?php
      }
     ?>     
    <p class="login-box-msg"><?php echo $heading ; ?></p>

    <form action="<?php echo base_url('login/verify') ; ?>" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="<?php echo $email_placeholder ; ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" value="<?php echo set_value('password'); ?>" class="form-control" placeholder="<?php echo $password_placeholder ; ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <a href="<?php echo base_url('forgot') ; ?>"><?php echo $forgot_text ; ?></a><br>
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="login" value="login" class="btn btn-primary btn-block btn-flat"><?php echo $button_text ; ?></button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<style type="text/css">
  
  body
  {
    height: auto;
  }

</style>