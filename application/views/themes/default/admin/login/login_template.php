<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Font Awesome -->

  <?php
    /** -- Copy from here -- */
    if(isset($meta) && count($meta)>0)
      foreach($meta as $name=>$content){
        echo "\n\t\t";
  ?>
        <meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" />
  <?php
      }
      echo "\n";

      if(isset($canonical) && $canonical!=="")
      {
        echo "\n\t\t";
  ?>
        <link rel="canonical" href="<?php echo $canonical?>" />
  <?php
      }
      echo "\n\t";

      foreach($css as $file){
        echo "\n\t\t";
  ?>
      <link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" />
  <?php
      } 
      echo "\n\t";  
    /** -- to here -- */
  ?>
</head>
<body class="hold-transition login-page">
<?php echo $output;
foreach($js as $file){
  echo "\n\t\t";
?>
  <script src="<?php echo $file; ?>"></script>
<?php
  } echo "\n\t";
?>
</body>
<!-- iCheck -->

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });  
</script>

</html>