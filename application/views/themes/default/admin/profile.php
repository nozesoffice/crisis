<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard') ; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <?php 
        if($this->session->flashdata('success')){
       ?>
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>Success !</strong> <?php echo $this->session->flashdata('success') ; ?>
      </div>
      <?php } ?>
      <?php 
        if($this->session->flashdata('error')){
       ?>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>Error !</strong> <?php echo $this->session->flashdata('error') ; ?>
      </div>
      <?php } ?>
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
                <?php 
                if(isset($user_details->userimage) && !empty($user_details->userimage))
                {
                  $path = FCPATH.'uploads/profile/'.$user_details->userimage;
                  if(file_exists($path))
                  {
                    $profile_img_path = base_url('uploads/profile/').$user_details->userimage;
                  }
                  else
                  {
                    $profile_img_path = base_url('uploads/profile/admin.png');
                  }
                }
                else
                {
                  $profile_img_path = base_url('uploads/profile/admin.png');
                }
               ?>
                <img src="<?php echo $profile_img_path; ?>" id="output" class="profile-user-img img-responsive img-circle" alt="User Image">
                <h3 class="profile-username text-center"><?php if(!empty($user_details->first_name) && !empty($user_details->last_name)){ echo strtoupper($user_details->first_name." ".$user_details->last_name);}else{ echo strtoupper($user_details->username); }?></h3>
              <p class="text-muted text-center"><?php echo isset($user_details->role_name) ? ucwords($user_details->role_name) : '' ;?></p>
              <ul class="list-group list-group-unbordered">
              </ul>
             <!--  <button type="submit" class="btn btn-primary btn-block"><b>Change Profile</b></button> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">              
              <li class="active"><a href="#settings" data-toggle="tab">Profile</a></li>
            </ul>
            <div class="tab-content">
              <div class=" active tab-pane" id="settings">
                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo base_url('profile/update'); ?>">
                  <input type="hidden" name="hidden_user_id" id="hidden_user_id" value="<?php echo $user_details->id; ?>">
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Company</label>
                    <div class="col-sm-10">
                      <input type="text" value="<?php echo $user_details->company; ?>" name="company" class="form-control" id="inputName" placeholder="Company Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"><span class="req">* </span>First Name</label>
                    <div class="col-sm-10">
                      <input type="text" name="first_name" value="<?php echo set_value('first_name') == false ? $user_details->first_name : set_value('first_name'); ?>" class="form-control" id="inputName" placeholder="First Name">
                      <?php echo form_error('first_name'); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"><span class="req">* </span>Last Name</label>
                    <div class="col-sm-10">
                      <input type="text" name="last_name" value="<?php echo $user_details->last_name; ?>" class="form-control" id="inputName" placeholder="Last Name">
                      <?php echo form_error('last_name'); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="user_name" class="col-sm-2 control-label"><span class="req">*</span>User Name</label>
                    <div class="col-sm-10">
                      <input placeholder="User name" type="text" value="<?php echo $user_details->username; ?>" class="form-control" id="user_name" name="user_name">
                      </div>
                    <?php echo form_error('user_name'); ?>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email Id</label>
                    <div class="col-sm-10">
                      <input readonly="" type="text" name="email" value="<?php echo $user_details->email; ?>" class="form-control" id="inputEmail" placeholder="Email Id">
                      <?php echo form_error('email'); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label"><span class="req">* </span>Contact No.</label>
                    <div class="col-sm-10">
                      <input type="text" name="contact" value="<?php echo $user_details->phone; ?>" class="form-control" id="inputName" placeholder="Contact No">
                      <?php echo form_error('contact'); ?>
                    </div>
                  </div>                  
                  <br>
                 
                  <h5>Profile Image</h5>
                  <hr>
                  <div class="form-group">
                <label for="file_path" class="col-sm-2 control-label">Profile Image</label>
                 <div class="col-sm-10">
                    <div class="input-group">
                        <input type="text" readonly="readonly" id="file_path" class="form-control" placeholder="Browse...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="file_browser">
                            <i class="fa fa-search"></i> Browse</button>
                        </span>
                    </div>
                    <input accept="image/*" onchange="loadFile(event)" type="file" class="hidden" id="file" name="userfile">
                </div>
            </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-info">Update</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<style type="text/css">
  .req {
    color: red;
  }
  .alert{
      /*width: 98%;
      margin-left: 1%;*/
  }
  .error {
  color:red;
  font-size:13px;
  margin-bottom:-15px;
  }
  .error-message{
    color:red;
  }
</style>

<script>
   $(document).ready(function(){  

      $('#file_browser').click(function(e){
          e.preventDefault();
          $('#file').click();
      });

      $('#file').change(function(){
          $('#file_path').val($(this).val());
      });

      $('#file_path').click(function(){
          $('#file_browser').click();
      });  
  });

  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>