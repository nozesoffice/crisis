<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

    <?php 
      if($success = $this->session->flashdata('success'))
      {
    ?>

    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>Fine !</strong> <?php echo $success ; ?>
    </div>

    <?php
      }
     ?>

     <?php 
      if($error = $this->session->flashdata('error'))
      {
    ?>

    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>Oops !</strong> <?php echo $error ; ?>
    </div>

    <?php
      }
     ?>
    <p class="login-box-msg"><?php echo $heading; ?></p>

    <form action="<?php echo base_url('reset_password/update') ; ?>" method="post">
      <div class="form-group has-feedback">
        <input value="<?php echo set_value('new_password') ; ?>" type="password" name="new_password" class="form-control" placeholder="<?php echo $new_password_placeholder; ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input value="<?php echo set_value('cnew_password') ; ?>" type="password" name="cnew_password" class="form-control" placeholder="<?php echo $confirm_new_password_placeholder; ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="row">
        <div class="col-xs-6">
          <a href="<?php echo base_url('login') ; ?>"><?php echo $login_text; ?></a><br>
    <!-- <a href="register.html" class="text-center">Register a new membership</a> -->
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" name="login" class="btn btn-primary btn-block btn-flat"><?php echo $update_password_button_text; ?></button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<style type="text/css">
  
  body
  {
    height: auto;
  }

</style>