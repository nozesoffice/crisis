<div class="content-wrapper">
<section class="content-header">
      <h1>System module access</h1>

     <ol class="breadcrumb">
       <?php foreach ($breadcrumb as $bkey => $bvalue) {
          ?>
            <li class="<?php echo $bvalue['class']; ?>">
              <?php if($bvalue['class']) { 
              echo $bvalue['text'];
               } else { ?>
              <a href="<?php echo $bvalue['href']; ?>"><?php echo $bvalue['text']; ?></a>
              <?php } ?>
            </li>
          <?php
          } ?>
      </ol>
      
    </section>


<section class="content">
      <div class="row">
        <div class="col-md-12">

        <!-- Success message -->

        <?php 

          if(isset($success))
          {
            $this->session->unset_userdata('success');
           ?>

            <div class="alert alert-success alert-dismissible" id="success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $success; ?>
            </div>

           <?php
          }

         ?>
      
    
         
     

      <!-- Success message -->

      <!-- error message -->

        <?php 

          if(isset($error))
          {
            $this->session->unset_userdata('error');

           ?>
     
          <div  class="alert alert-danger alert-dismissible" id="error">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $error; ?>
            </div>

            <?php 
          }
             ?>
     
              <a href="<?php echo base_url('roles'); ?>" class="btn btn-primary pull-right" title="Back"><i class="fa fa-reply"></i></a>
                <br><br>
       <!-- error message -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Role Access</h3>
              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <!-- <button type="button" style="margin:10px;" class="btn btn-info" data-toggle="modal" data-target="#groups_modal">Save Settings</button> -->
            <div class="row" style="margin:0 ;">
                <div class="col-md-12">
                  <div class="title-bg">Role Name : <?php echo ucwords($role_data['role_name']); ?></div>
                </div>
                <div class="col-sm-12">
                    <div class="box-body" style="padding: 0px;">

                    <form action ="<?php echo base_url($action)?>" role="form" class="form-validation" id="frmvalidate" method="post">

                       <input type="hidden" name="role_id" value="<?=isset($role_data['role_id'])?$role_data['role_id']:''?>">

                      <div class="table-responsive">
                        <table class="table no-margin">
                          <tbody>
                            <tr>
                                <th>Module Name</th>
                                <th colspan="5">Actions</th>
                            </tr>
                            <?php 
                              foreach ($system_modules as $key => $modules) {
                             ?>
                            <tr> 
                              <td><b><?php echo $modules['module_name']; ?></b></td>
                              <td>
                                <label class="switch">
                                  <input type="checkbox" id="<?php echo $key; ?>" class="all" name="all" value="<?php echo $modules['module_id']; ?>">
                                  <span class="slider round"></span>
                                </label> &nbsp;
                                All      &nbsp;&nbsp;
                                <?php 
                                $operations      = $modules['operation'];
                                $data            = explode("|",$operations);
                                $total_operation = count($data);
                                $count_operation = 0;
                                foreach ($data as $k => $value) 
                                {
                                  $access_value = $modules['controller_name']."/".$value; ?>
                                  <label class="switch list">
                                    <input  type="checkbox" <?php if (in_array($access_value, $group_access)) { echo 'checked="checked"'; $count_operation++; }?>  class="<?php echo $modules['module_id']; ?>"  name="Access[]" value="<?php echo $access_value ; ?>">
                                    <span class="slider round"></span>
                                  </label> &nbsp; 
                                    <?php 
                                     if($value == 'manage_event')
                                     {
                                        echo "Manage";
                                     }
                                     else if($value == 'manage_program')
                                     {
                                        echo "Manage";
                                     }
                                     else if($value == 'viewevent')
                                     {
                                        echo "View";
                                     }
                                     else if($value == 'addevent')
                                     {
                                        echo "Add";
                                     }
                                     else if($value == 'editevent')
                                     {
                                        echo "Edit";
                                     }
                                     else if($value == 'delete' || $value == 'deleteevent')
                                     {
                                        echo "Delete";
                                     }
                                     else if($value == 'view_is_deleted')
                                     {
                                        echo "Show Is Delete";
                                     }
                                     else if($value == 'delete_payment_records' || $value == 'event_delete_payment_records')
                                     {
                                        echo "Delete Payment Records";
                                     }                              
                                     else 
                                     {
                                        echo ucwords($value);
                                     }
                                    ?>&nbsp;&nbsp;
                                  <?php 
                                }

                                //Checked All Button
                                if($total_operation == $count_operation)
                                {
                                  ?>
                                  <script type="text/javascript">
                                    $("#<?php echo $key; ?>").attr("checked", true);
                                  </script>
                                  <?php
                                 } ?>                           
                              </td>                            
                            </tr> 
                            <?php 
                              }
                             ?>

                           <tr>
                             <td colspan="8"></td>
                           </tr>
                          </tbody>  
                        </table> 

                        <input type="submit" name="submit" value="Save" style="margin:10px;" class="btn btn-info">
                        


                      </div>
                    </form>
                    </div>
                </div>
            </div>
           </div> 
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>

    </div>


    <style type="text/css">
.title-bg{
  background: #00c0ef;
  padding: 10px;
  color: #fff;
  font-weight: bold;
  border-radius: 2px;
  margin: 5px 0;
}    
.req {
  color: red;
}
.alert{
    width: 98%;
    margin-left: 1%;
}
.error {
color:red;
font-size:13px;
margin-bottom:-15px;
}
.error-message{
  color:red;
}
 /* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 50px;
  height: 25px;
  vertical-align: top;
}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 18px;
  width: 18px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<script type="text/javascript">
 $('[data-toggle="tooltip"]').tooltip();

$('.all').change(function() {
        
            $("."+$(this).val()).prop('checked', $(this).prop("checked"));
 });


$('.list input[type="checkbox"]').on('change', function () {
  var allChecked = $('.list input:checked').length === $('.list input').length;

  $('.all').prop('checked', allChecked);
});

</script>