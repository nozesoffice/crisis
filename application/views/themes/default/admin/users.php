<div class="content-wrapper">

<section class="content-header">
      <h1>All Users</h1>

     <ol class="breadcrumb">

      <?php foreach ($breadcrumb as $bkey => $bvalue) {
          ?>
            <li class="<?php echo $bvalue['class']; ?>">
              <?php if($bvalue['class']) { 
              echo $bvalue['text'];
               } else { ?>
              <a href="<?php echo $bvalue['href']; ?>"><?php echo $bvalue['text']; ?></a>
              <?php } ?>
            </li>
          <?php
          } ?>
       
      </ol>
      
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">

        <!-- Success message -->
    
        <?php if ($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" id="success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php endif; ?>
     

      <!-- Success message -->

      <!-- error message -->
     
        <?php if ($this->session->flashdata('error')): ?>
          <div class="alert alert-danger alert-dismissible" id="error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
     

       <!-- error message -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Users</h3> 
              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <a href="<?php echo base_url('users/add');?>" class="btn btn-info AddItem">Add User</a>
              <div class="dropdown Action_Dropdown">
                 <a class="dropdown-toggle Action_custom btn btn-default" data-toggle="dropdown" href="#" aria-expanded="false">
                      Action
                      <span class="fa fa-caret-down"></span>
                  </a>
                <ul class="dropdown-menu Action_menu">
                  <li> <a href="#" class="action-delete" 
                          onClick="return action_delete(document.recordlist)" >
                          Delete
                          </a></li>
                  <li><a href="#" class="action-delete" 
                          onClick="return action_active(document.recordlist)" >
                          Active
                          </a></li>
                  <li><a href="#" class="action-delete" o
                          onClick="return action_deactive(document.recordlist)">
                          Deactive
                          </a></li>
                </ul>
              </div>

            <div class="row" style="margin:0;">
                <div class="col-sm-12">
                    <div class="box-body">
                      <div class="table-responsive">
                      <form name="recordlist" id="mainform"  method="post" action="<?php echo base_url('admin/users/action') ?>">
                        <table class="table no-margin" id="example1">
                          <thead>
                          <tr>
                            <th class="text-center" width="5%"><input class="text-center" type="checkbox" name="allchk" onclick="$('.chk').prop('checked', $(this).prop('checked'));"></th>                           
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Roles</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                          </thead>
                          <tbody>
                          
                          <?php 

                          if(isset($users)){
                            $i=1;
                            foreach ($users as $user) {
                              
                           ?>                    
                          <tr>
                            <td width="5%" valign="middle"><input type="checkbox" class="chk" name="Id_List[]" id="Id_List[]" value="<?php echo $user['id']?>"></td>
                            <td><a href="<?php echo base_url('users/edit/'.$user['id']) ;?>"  > <?php echo $user['first_name']; ?></a></td>
                            <td><?php echo $user['last_name']; ?></td>
                            <td><?php echo $user['username']; ?></td>
                            <td><?php echo $user['email']; ?></td>
                            <td><?php echo $user['phone']; ?></td>
                            <td><?php echo $user['role_name']; ?></td>
                            <td><?php echo ($user['active'] == 1) ? "Active" : "Deactivate"; ?></td>
                            <td>
                              <a href="<?php echo base_url('users/edit/'.$user['id']) ;?>" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></a>
                              <a onclick="delete_user('<?php echo $user['id'] ?>')" class="btn btn-danger" title="Edit"><i class="fa fa-trash-o"></i></a>
                            </td>
                          </tr>
                          <?php                           
                            } }
                           ?>                                                                  
                          </tbody>
                        </table>
                         <input type="hidden" name="action" />  
                        </form>
                      </div>
                    </div>
                </div>
            </div>
           </div> 
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
  </div>


    <style type="text/css">
.req {
  color: red;
}

.error {
color:red;
font-size:13px;
margin-bottom:-15px;
}
.error-message{
  color:red;

}
</style>



<script type="text/javascript">

function delete_user(user_id)
{
  var c = window.confirm('Are you sure want to delete  ?');

  if(c)
  {
    window.location.href = '<?php echo base_url('users/delete') ?>/'+user_id;
  }
  
}
</script>

<script>
  $(function () {
    $("#example1").DataTable({
      "aaSorting": [],
      language: {
        paginate: {
          next: 'Next', // or '→'
          previous: 'Previous' // or '←' 
        }     
      },
      "columnDefs": [
          { "orderable": false, "targets": 0 },
          { "searchable": false, "targets": 0 },
          { "orderable": false, "targets": 8 },
          { "searchable": false, "targets": 8 },
        ],
      "pageLength": 10,
    });
  });
</script>


<script>
 jQuery(document).ready(function(){
   jQuery('.Action_custom').click(function(){
   jQuery('.Action_menu').toggle('slow');
   });
 })
</script>



<script type="text/javascript">
  //==========================================================================

function CheckUncheckAll()
{
    var checks = document.getElementsByName('Id_List[]');
    for (i = 0; i < checks.length; i++)
    {
        if(checks[i].checked == true) 
            checks[i].checked = false;
        else
            checks[i].checked = true;
    }
}
//====================================================================
function action_active(frm)
{
    with(frm)
    {
        var flag = false;
        str = '';
        field = document.getElementsByName('Id_List[]');
        for (i = 0; i < field.length; i++)
        {
            if(field[i].checked == true)
            { 
                flag = true;
                break;
            }
            else
                field[i].checked = false;
        }
        if(flag == false)
        {
            alert("Please select atleast one record");
            return false;
        }
    }
    frm.action.value = "action_active";
    frm.submit();
    return true ;
}
//================================================================
function action_deactive(frm)
{
    with(frm)
    {
        var flag = false;
        str = '';
        field = document.getElementsByName('Id_List[]');
        for (i = 0; i < field.length; i++)
        {
            if(field[i].checked == true)
            { 
                flag = true;
                break;
            }
            else
                field[i].checked = false;
        }
        if(flag == false)
        {
            alert("Please select atleast one record");
            return false;
        }
    }
    frm.action.value = "action_deactive";
    frm.submit();
    return true ;
}
//===================================================================
function action_delete(frm)
{
    with(frm)
    {
        var flag = false;
        str = '';
        field = document.getElementsByName('Id_List[]');
        for (i = 0; i < field.length; i++)
        {
            if(field[i].checked == true)
            { 
                flag = true;
                break;
            }
            else
                field[i].checked = false;
        }
        if(flag == false)
        {
            alert("Please select atleast one record");
            return false;
        }
    }
    if(confirm("Are you sure to delete selected records ?"))
    {
        frm.action.value = "action_delete";
        frm.submit();
        return true ;
    }
}
</script>

