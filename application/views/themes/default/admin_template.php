<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/custom.css">

  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/plugins/morris/morris.css">
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/plugins/jvectormap/jquery-jvectormap-1.2.2.css"> -->
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/plugins/daterangepicker/daterangepicker.css">
  <!-- Time picker -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/dist/css/dropzone.css">
  
  <!-- jQuery 2.2.3 -->
  <script src="<?php echo ADMIN_PATH;?>default/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>

  <!-- Select 2 -->
  <link rel="stylesheet" href="<?php echo ADMIN_PATH;?>default/admin/plugins/select2/select2.min.css">
  <script src="<?php echo ADMIN_PATH;?>default/admin/plugins/select2/select2.min.js"></script>




  <!-- DropZone -->
  <script src="<?php echo ADMIN_PATH;?>default/admin/js/dropzone.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('dashboard'); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>VPN</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>VPN</b>Infotech</span>
    </a>

   <?php $user_details = $this->common->get_userdata($this->session->userdata('email')) ;?>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <select name="lang" id="lang">
        <option value="english" <?php if ($this->session->userdata('lang') == 'english') { echo 'selected'; } ?>>English</option>
        <option value="gujarati" <?php if ($this->session->userdata('lang') == 'gujarati') { echo 'selected'; } ?>>Gujarati</option>
      </select>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <?php 
                if(isset($user_details->userimage) && !empty($user_details->userimage))
                {
                  $path = FCPATH.'uploads/profile/'.$user_details->userimage;
                  if(file_exists($path))
                  {
                    $profile_img_path = base_url('uploads/profile/').$user_details->userimage;
                  }
                  else
                  {
                    $profile_img_path = base_url('uploads/profile/admin.png');
                  }
                }
                else
                {
                  $profile_img_path = base_url('uploads/profile/admin.png');
                }
               ?>
                <img src="<?php echo $profile_img_path; ?>" class="user-image" alt="User Image">
             <span class="hidden-xs"><?php if(!empty($user_details->first_name) && !empty($user_details->last_name)){ echo strtoupper($user_details->first_name." ".$user_details->last_name);}else{ echo strtoupper($user_details->username); }?>
             </span> 
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $profile_img_path; ?>" class="img-circle" alt="User Image">
                <p><?php if(!empty($user_details->first_name) && !empty($user_details->last_name)){ echo strtoupper($user_details->first_name." ".$user_details->last_name);}else{ echo strtoupper($user_details->username); }?>
                  <small><?php echo isset($user_details->email) ? $user_details->email : '' ; ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer" style="padding: 3px;">
                <div class="pull-left">
                  <a href="<?php echo base_url('profile') ?>" class="btn btn-default btn-flat">Profile</a>
                  <a href="<?php echo base_url('change_password') ?>" class="btn btn-default btn-flat">Change Password</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!--  -->

      <div class="user-panel">
        <div class="pull-left image">
          <img style="height: 45px" src="<?php echo $profile_img_path; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
         <p><?php if(!empty($user_details->first_name) && !empty($user_details->last_name)){ echo strtoupper($user_details->first_name." ".$user_details->last_name);}else{ echo strtoupper($user_details->username); }?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?php echo isset($user_details->role_name) ? ucfirst($user_details->role_name) : '' ;?></a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview <?php if($this->uri->segment(1) == 'dashboard') { echo 'active'; } ?>">
          <a href="<?php echo base_url('dashboard')?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>        
        <li class="treeview <?php if($this->uri->segment(1) == 'email_template') { echo 'active'; } ?>">
          <a href="<?php echo base_url('email_template'); ?>">
            <i class="fa fa-envelope-o"></i>
            <span>Email template</span>
            <span class="pull-right-container">
            
            </span>
          </a>          
        </li>
        <li class="treeview <?php if($this->uri->segment(1) == 'users') { echo 'active'; } ?>">
          <a href="<?php echo base_url('users'); ?>">
            <i class="fa fa-users"></i>
            <span>Manage User</span>
            <span class="pull-right-container">            
            </span>
          </a>          
        </li>

        <li class="treeview <?php if($this->uri->segment(1) == 'roles') { echo 'active'; } ?>">
          <a href="<?php echo base_url('roles'); ?>">
            <i class="fa fa-user-secret"></i>
            <span>All Roles</span>
            <span class="pull-right-container">            
            </span>
          </a>          
        </li>

        <li class="treeview <?php if($this->uri->segment(1) == 'cms') { echo 'active'; } ?>">
          <a href="<?php echo base_url('cms'); ?>">
            <i class="fa fa-bolt"></i>
            <span>CMS</span>
            <span class="pull-right-container">            
            </span>
          </a>          
        </li>

        <li class="treeview <?php if($this->uri->segment(1) == 'change_password') { echo 'active'; } ?>">
          <a href="<?php echo base_url('change_password'); ?>">
            <i class="fa fa-key" aria-hidden="true"></i>
            <span>Change Password</span>
            <span class="pull-right-container">            
            </span>
          </a>          
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <?php echo $output;?>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">

      <!-- <b>Version</b> 2.3.8 -->
    </div>
    <strong>Copyright &copy; 2017 VPN INFOTECH. </strong> All rights
    reserved.

  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo ADMIN_PATH;?>default/admin/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="<?php echo ADMIN_PATH;?>default/admin/plugins/morris/morris.min.js"></script> -->
<!-- Sparkline -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- timepicker -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo ADMIN_PATH;?>default/admin/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo ADMIN_PATH;?>default/admin/dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo ADMIN_PATH;?>default/admin/dist/js/demo.js"></script>
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo ADMIN_PATH;?>default/admin/plugins/datatables/dataTables.bootstrap.min.js">
</script>
<script type="text/javascript">
$(document).ready(function(){
    $("#lang").on("change", function(e) {
      
      // console.log('e',e);
    e.preventDefault();
      // alert('sdf');
      var id = $("#lang").val();
        $.ajax({
          url: '<?php echo base_url('users/lang') ?>',
          type: 'POST',
          data: {
              key: id
          },
          success: function(data) {
                console.log('data',data);
              location.reload();
             
          }
      });
         // ev.preventDefault();
    });
});
</script>
</body>
</html>
